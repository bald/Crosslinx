# -*- mode: python -*-

import platform
block_cipher = None


a = Analysis(['crosslinx.py'],
             pathex=['./include/python/'],
             binaries=None,
             datas=None,
             hiddenimports=["tempfile", "json", "subprocess", "mpmath", "xml.etree.cElementTree", "platform"],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
##### include mydir in distribution #######
def extra_datas(mydir):
    def rec_glob(p, files):
        import os
        import glob
        for d in glob.glob(p):
            if os.path.isfile(d):
                files.append(d)
            rec_glob("%s/*" % d, files)
    files = []
    rec_glob("%s/*" % mydir, files)
    extra_datas = []
    for f in files:
        extra_datas.append((f, f, 'DATA'))

    return extra_datas
###########################################

# append the 'data' dir
a.datas += extra_datas('include')
a.datas += extra_datas('helper')
a.datas += extra_datas('tests') #just needed to run tests with the executable

# exclude ruby dirs that are not needed on different operating systems:
if platform.system() == "Windows":
    a.datas = [x for x in a.datas if not x[0].startswith("helper\\ruby\\traveling-ruby-20150715-2.2.2-linux-x86_64")]
    a.datas = [x for x in a.datas if not x[0].startswith("helper\\ruby\\traveling-ruby-20150715-2.2.2-osx")]
elif platform.system() == "Linux":
    a.datas = [x for x in a.datas if not x[0].startswith("helper/ruby/ruby-1.9.1-minimal-windows")]
    a.datas = [x for x in a.datas if not x[0].startswith("helper/ruby/traveling-ruby-20150715-2.2.2-osx")]
    a.datas = [x for x in a.datas if not x[0].startswith("helper/7zip")]
elif platform.system() == "Darwin":
    a.datas = [x for x in a.datas if not x[0].startswith("helper/ruby/ruby-1.9.1-minimal-windows")]
    a.datas = [x for x in a.datas if not x[0].startswith("helper/ruby/traveling-ruby-20150715-2.2.2-linux-x86_64")]
    a.datas = [x for x in a.datas if not x[0].startswith("helper/7zip")]

pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='crosslinx',
          debug=False,
          strip=False,
          upx=True,
          console=True )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               name='crosslinx')
