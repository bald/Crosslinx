Please refer to the [readme](https://gitlab.com/bald/Crosslinx/blob/master/README.md) for a more general help. 
This page lists all options that are available in Crosslinx.

Masses and tolerance
--------------------

* `-ppmMS1 <float>` (default: 5.0)   
Specify the mass accuracy for identifying precursor ions of crosslinked peptides in MS1 spectra.

* `-ppmMS2 <float>` (default: 20.0)  
Specify the mass accuracy for assigning in silico fragmented peptides to MS2 spectra.

* `-cShift <flag>` (default: false)     
All cysteines in a sequence are considered as having the modification

Enzyme specification
--------------------

* `-enzyme <string>` (default: Trypsin (RK|))  
Specify the enzyme used for in silico digestion of the protein database.

* `-notCleavedAminoAcids <string>` (default: Trypsin (P))  
Specify the amino acids before a cleavage site, after which the enzyme does not cut.

* `-missedCleavageSites <int>` (default: 3)   
Specify the number of missed cleavages for in silico digestion of the protein database.

* `-minPeptideLength <int>` (default: 5)   
Minimum peptide length

* `-18O <flag>` (default: false)   
digestion with 18O water, labelling of c terminus (+ 2.004 Da.)

Cross-linker specification
--------------------------

* `-crossLinkerMass <string>` (default: 138.0680796 (DSSd0))    
Enter the mass here, optionally followed by a space and some descriptive text.

* `-crossLinkerMassShift <string>` (default: 4.025107 (d0/d4))   
Enter the mass shift here, optionally followed by a space and some descriptive text.

* `-crosslinkedAA <string>` (default: K (DSS and BS3))   
Enter the amino acids which are crosslinked, optionally followed by a space and some descriptive text. N-terminus indicated by "_"

Ions to search
--------------

* `-ionsToSearch <a, b, c, x, y, z>` (default: b,y)   
Ions to search

* `-calculateLosses <flag>` (default: false)   
Calculate neutral losses

MS2 handling
------------

* `-deconvolution <flag>` (default: true)   
Deconvolution (determince uncharged masses from m/z values)

* `-removeNoise <flag>` (default: true)
Remove noise from spectrum

Scoring
-------

* `-minimumMatches <int>` (default: 6)   
Minimum matching peaks (spectrum)

Isotopic distribution
---------------------

* `-maxIsotopeNumber <int>` (default: 0)   
maximum isotope number that is allowed in MS1 scans. (0 = monoisotopic)

* `-MS2Isotope <string>` (default: monoisotopic)   
Product ion (MS2) search type.

target/decoy
------------

* `-decoy_on_demand <flag>` (default: false)   
Build decoy peptides (reverse, keep end) on demand for every target
combination that matches in MS1 scans. No target/decoy database is required.

* `-td_prefix_decoy <string>` (default: `__td__decoy_`)   
Specify prefix for decoy sequence identifiers.

* `-td_prefix_target <string>` (default: `__td__target_`)   
Specify prefix for target sequence identifiers.

Tweaks
------

* `-cache <flag>` (default: false)   
Use caching for MS2 fragment ions. Should be used with small protein
databases on high number of spectra. Enabling this option when using a
large database may use VERY much RAM!

Output
------

* `-plot <flag>` (default: false)   
Creates a plot for every MS2 scan with hits including the
corresponding MS1 scan. Plots are stored in the output directory

Output files
------------

* `-outputDirectory <string>` (default: )   
Output directory

* `-outputPrefix <string>` (default: )   
Output file prefix

* `-outputWriteIdentified_crosslinks_perScan <flag>` (default: yes)   
Write identified cross-linked PSM (crosslinx-results.csv)

Input files
-----------

- at least 1 protein database A file   
  format: FASTA (.fasta|.fas|.fa|.faa|.fna|.ffn|.frn)


- at least 1 protein database B file   
  format: FASTA (.fasta|.fas|.fa|.faa|.fna|.ffn|.frn)


- at least 1 mzML file   
  format: mzML (.mzml), mzML (compressed) (.mzml.zip|.mzml.gz|.mzml.bz2)


- at least 0 PSM files   
  format: Comma separated values (.csv)


- at least 0 cross-linked PSM files   
  format: Comma separated values (.csv)


Input file ambiguities
----------------------

Because some input file formats appear in multiple input file groups,
files in some input formats must be manually assigned to a certain
input file group by preceding the filenames with the appropriate
switches.

Affected input file formats:

- Comma separated values (.csv)

- FASTA (.fasta|.fas|.fa|.faa|.fna|.ffn|.frn)

Input file group assignment switches:

* `-fastaFilesA`:   
subsequent files are interpreted as protein database A
files

* `-fastaFilesB`:   
subsequent files are interpreted as protein database B
files

* `-omssaFiles`:   
subsequent files are interpreted as PSM files

* `-crosslinkFiles`:   
subsequent files are interpreted as cross-linked PSM
files

Output directory
----------------

Unless an output directory is specified, the output files will be
written to the directory of the first mzML file.
