This folder is the output folder for all test files.
It is added to the git repository to allow uploading the test results as an artifact.zip during gitlab CI.
This readme.txt is necessary, so that the folder is not empty and is subsequently added to the dist folder during pyinstaller execution.