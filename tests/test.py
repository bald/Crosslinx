# run some tests for crosslinx
# be sure to execute this in the scripts folder
# python3 tests/test.py

import unittest
import sys
import os
import tempfile
import hashlib
import subprocess

sys.path.append('.')

#          FOLDERNAME;            sha256 sum of the expected output file
cases = (('case1', 'c99df50a89f6c845f0a738cb4e6d679f363c6c9eb2049431dfeaa0235cc935c1'),         # HX_Crosslinks-27052014-NEU_6-15-4021MS2
         #('case2', '866f3cd71add1df2ff0e4bf1e397229227e02497e9db2040ff5e948823bec57b'))         # HX_Crosslinks-27052014-NEU_6-15-s4020MS1-4021MS2
         ('case2', 'ed0d175a04d50b0b1b80562f689d642404aa6326837532316b93496a102a7b37'),         # HX_Crosslinks-27052014-NEU_6-15-s4020MS1-4021MS2
         ('case3', '4b917fdb0194e6a3c99e285c36ddac0a071b233148f5d0c4fe4ae289318c2028'),         # HX_Crosslinks-27052014-NEU_6-15-4021MS2 no deconvolution
         ('case4', 'c99df50a89f6c845f0a738cb4e6d679f363c6c9eb2049431dfeaa0235cc935c1'),         # HX_Crosslinks-27052014-NEU_6-15-4021MS2 different fasta files
         ('case5', '__string__Found 3/99 which are too big, even if they are not crosslinked'), # HX_Crosslinks-27052014-NEU_6-15-4021MS2 different fasta files, sames protein names, too big peptides
         ('case6', ('6b371f0d14c428adc9ab0f8445756cad041b4000727fa4d8d8b11c6b397998d7',
                    'b80058d096120a42c5f61eddb610950d11aa35209f7881690a58e6a508a0d699',
                    '084f2caf1312961646e953841500def0eed52ae846dd960697ffe07cf56d7d6e')), # HX_Crosslinks-27052014-NEU_6-15-s4020MS1-4021MS2 plotting, no deconvolution
         ('case7', ('542264e31111629062bcd4a22808daf41f3f352fa1e1af9d8b0cddec87a4873d',
                    'f17513a9ae4efe2dbe67e55ee14dc35f71b748df57690e2dbbe8b381de0872be',
                    'a20f5a635d152ea33453aa89d6048d3928e89c409853df932fbc11a53dc3f8bb')), # HX_Crosslinks-27052014-NEU_6-15-s4020MS1-4021MS2 plotting, no deconvolution, with noise removal
         ('case8', '4e1de1d6d8c2d2be9d6a91e4b10889fe6fdb3b425dff9e9aae69df2e239514d6'),         # HX_Crosslinks-27052014-NEU_6-15-4021MS2, d12 only, denoted as 'light' in output
         ('case9', ('0dedc0d023062878dfe022ca958df13789fc2030239c548246837866dfe26352',
                    '4980a714bd39d7500ba66795d6428a9d5609c31cd2365561193edc32abcad57c',
                    '34543c775e0c58e49868bed387b2dfb6257349ee0917f6e1961287424ddaeacf',
                    '11c6c788e998c73181f9aa7ed15c8cb7db0acb3334cb6b8ab03cb9bf96554b35')),         # HX_Crosslinks-27052014-NEU_4-1-severalMS2, MS2 isotope 2, skipping of PSM scans
         ('case10', 'c99df50a89f6c845f0a738cb4e6d679f363c6c9eb2049431dfeaa0235cc935c1'),        # HX_Crosslinks-27052014-NEU_6-15-4021MS2 + another MS2 scan, skipping of cPSM scans
         ('case11', 'dcd719389eea2310846b48a6c07ec2b4dd0156695a8789cdc27af6a48dd90fa7'),         # HX_Crosslinks-27052014-NEU_6-15-4021MS2, decoy on demand
         ('case12', ('ed0d175a04d50b0b1b80562f689d642404aa6326837532316b93496a102a7b37',
                    '...',
                    '...')), # HX_Crosslinks-27052014-NEU_6-15-s4020MS1-4021MS2 plotting with deconvolution
         ('case13', '6a39efb6905a6eb155b075d1f6fb8be9aacc6dd686707db8d7d169b2d2272387'),         # HX_Crosslinks-27052014-NEU_6-15-4021MS2, all ions, with losses
        )  
         # missing tests:
         # plotting
         # no hits
         # several spectra 

class TestCenter(unittest.TestCase):
    script_folder = os.path.abspath('.')
    

def create_test(exec_type, case, output_sha256_expected):

    def do_test(self):
        # this tests that different input combinations generate the right output
        # input has to be stored in the following folder
        # crosslinx/tests/

        #print('/' * 80)
        #print(case)
        #print('/' * 10)
        # use all files available as an input
        
        case_folder = os.path.join(self.script_folder, 'tests', case)
        input_files_path = {}
        input_files = {'fastaFilesA': 'protein_database_a_input.fasta',
                       'fastaFilesB': 'protein_database_b_input.fasta',
                       'omssaFiles': 'psm_input.csv',
                       'crosslinkFiles': 'crosslink_input.csv',
                       'mzMLFiles': 'spectra_input.mzML',            # only one of these [mzML or mzML.gz] is tested.
                       'mzMLFiles.gz': 'spectra_input.mzML.gz'}
        for file_key, file_name in input_files.items():
            file_path = os.path.join(case_folder, file_name)
            if os.path.isfile(file_path):
                # convert file_path for ruby - it wont work with \ on windows
                if '\\' in file_path:
                    file_path = '/'.join(file_path.split('\\'))
                if file_key == 'mzMLFiles.gz':
                    input_files_path['mzMLFiles'] = file_path
                else:
                    input_files_path[file_key] = file_path
        
        # read params from parameter file:
        params = []
        with open(os.path.join(case_folder, 'parameter.txt'),'r') as parameter_file:
            for line in parameter_file:
                params.append(line.rstrip())
        
        output_prefix = 'test_'
        # output files are stored in a subfolder to allow uploading artifacts 
        # during gitlab CI
        temp_basedir = os.path.join(self.script_folder, 'tests', 'test_output')
        # convert file_path for ruby - it wont work with \ on windows
        if '\\' in temp_basedir:
          temp_basedir = '/'.join(temp_basedir.split('\\'))        
        
        output_directory = tempfile.mkdtemp(prefix = '{0}_test_{1}_'.format('crosslinx', case), dir = temp_basedir)
        # output files are not removed automatically to allow the user to inspect the files
        
        script_arguments = params + ['-outputPrefix', output_prefix, '-outputDirectory', output_directory]
        # add all other files
        for param_key, path in input_files_path.items():
            script_arguments.append('-{0} {1}'.format(param_key, path))
            
        
            
        cmd = '{0} {1}'.format(exec_type, ' '.join(script_arguments))
        p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
        output, errors = p.communicate()

        # generate possible error message
        error_message = '\nfile type: {0}\n\nOutput can be found in: {1}\n\nScript Output: {2}\n\nScript Errors: {3}'.format('crosslink-results.csv', output_directory, output, errors)

        # get output filenames
        output_path = os.path.join(output_directory, ''.join((output_prefix, 'crosslinx-results.csv')))


        if type(output_sha256_expected) is tuple:
            # check that the output file exists
            self.assertEqual(os.path.isfile(output_path), True, error_message)
            result = False
            with open(output_path, 'rb') as output_file:
                output_file_sha256 = hashlib.sha256(output_file.read()).hexdigest()
                for single_output_sha256_expected in output_sha256_expected:
                    # we might have different checksums on different systems, so check that at least one of those is the actual result
                    # check that output has the right checksum
                    result = output_file_sha256 == single_output_sha256_expected
                    if result == True:
                        # we found a valid result - no need to check further
                        break
            # assert equal
                self.assertEqual(result, True, 'sha256sum of the actual result is {0}\n\n{1}'.format(output_file_sha256, error_message))
        elif output_sha256_expected == '':
            # check that the file does NOT exist.
            self.assertEqual(os.path.isfile(output_path), False, error_message)
        elif output_sha256_expected.startswith('__string__'):
            # check for output messages instead of output file
            output_str_expected = output_sha256_expected.split('__string__')[-1]
            self.assertEqual(output_str_expected in str(output), True, error_message)

        elif output_sha256_expected.startswith('__plot__'):
            # check that the plotted spectra file exists
            output_path_plot = os.path.join(output_directory, ''.join((output_prefix, 'crosslinx-results_spectra_input_4021.xhtml')))
            self.assertEqual(os.path.isfile(output_path_plot), True, error_message)
            # compare files directly - sha256sum does not work due to different newline characters
            self.assertEqual(compare_files(output_path_plot, os.path.join(case_folder, 'expected_plotting result.xhtml')), True, error_message)
        else:
            # check that the output file exists
            self.assertEqual(os.path.isfile(output_path), True, error_message)

            # check that output has the right checksum
            with open(output_path, 'rb') as output_file:
                output_file_sha256 = hashlib.sha256(output_file.read()).hexdigest()
                self.assertEqual(output_file_sha256, output_sha256_expected, error_message)
    
    return do_test

def compare_files(filepath1, filepath2):
    # compare two files line by line
    # returns True if the files are the same, ignoring newline differences
    # returns False immediately if a difference is detected.
    with open(filepath1, 'r') as file1, open(filepath2, 'r') as file2:
        for line1, line2 in zip(file1, file2):
            if line1 != line2:
                return False
        return True
                
            

if __name__ == '__main__':
    # there are two types of tests, one runs directly on the python script,
    # the other uses the crosslinx executable created with pyinstaller
    # When using the test script from CI, make sure to
    # 1. Test the native python script (call python crosslinx.py via system call)
    # 2. Let CI build the executable
    # 3. Test the executable

    # read exec type from command line:
    # use CI to distinguish OS and executable or script.
    # use CI to cd into script/executable directory
    # for windows use either
    #   "python crosslinx.py"
    #   "dist/crosslinx/crosslinx.exe"
    # on linux use either
    #   "python3 crosslinx.py"
    #   "./dist/crosslinx/crosslinx"
    
    # default exec_type would be linux script
    exec_type = 'python3 crosslinx.py'
    speed = 'normal'
    if len(sys.argv) > 1 and 'crosslinx' in sys.argv[1]:
        exec_type = sys.argv[1]
        del sys.argv[1]
    if len(sys.argv) > 1 and 'speed fast' in sys.argv[1]:
        speed = 'fast'
        del sys.argv[1]

    for (case, output_sha256_expected) in cases:
        if case == 'case9' and speed == 'fast':
            continue
            # skip this test on slow machines
        test_method = create_test(exec_type, case, output_sha256_expected)
        test_method.__name__ = 'test_{0}'.format(case)
        setattr(TestCenter, test_method.__name__, test_method)
    unittest.main()
