# Crosslinx
[![build status](https://gitlab.com/bald/Crosslinx/badges/master/build.svg)](https://gitlab.com/bald/Crosslinx/commits/master)
[![coverage report](https://gitlab.com/bald/Crosslinx/badges/master/coverage.svg)](https://gitlab.com/bald/Crosslinx/commits/master)
[![pylint Score](https://mperlet.github.io/pybadge/badges/9.23.svg)](https://gitlab.com/bald/Crosslinx/commits/master)

Identify cross-linked peptides in MS/MS data. Crosslinx is written in Python (version 3) and can be used as a script if Python is installed. Crosslinx is also available as a standalone program, if Python is not installed.


# Use
Standalone versions of Crosslinx can be used on Windows and Linux via the command line interface. There is no need to install additional progams.

For other operating systems, such as Mac OSX, Crosslinx is available as a Python script. Please make sure that the [Python programming language](https://www.python.org/) (version 3.x) is installed.

If a graphical user interface is prefered, [Proteomatic](http://www.uni-muenster.de/hippler/proteomatic/) can be used. Please follow the instructions for [proteomatic-scripts-crosslinx](https://gitlab.com/bald/Crosslinx/blob/master/PROTEOMATIC-SCRIPTS-CROSSLINX.MD).


## Download Crosslinx
Download Crosslinx from the links below and unzip the file to any directory.

[Crosslinx Standalone for Windows](https://gitlab.com/bald/Crosslinx/builds/artifacts/0.7.2/download?job=release-win10)

[Crosslinx Standalone for Linux](https://gitlab.com/bald/Crosslinx/builds/artifacts/0.7.2/download?job=release-ubuntu64)

[Crosslinx source code](https://gitlab.com/bald/Crosslinx/repository/archive.zip?ref=0.7.2)

## Running Crosslinx
Execute Crosslinx on the command line in the directory where you unzipped it.
```
cd crosslinx_0.7.2
./crosslinx --help # on Linux
crosslinx.exe      # on Windows
```

## Options
Execute `./crosslinx --help` to show the help. See [options](https://gitlab.com/bald/Crosslinx/blob/master/OPTIONS.MD) for a detailed list of available options.


# Citation
Please cite the following publication:

> Shinichiro Ozawa, Till Bald, Takahito Onishi, Huidan Xue, Takunori Matsumura, Ryota Kubo, Hiroko Takahashi, Michael Hippler, Yuichiro Takahashi:  
> Configuration of ten light-harvesting chlorophyll a/b complex I subunits in *Chlamydomonas reinhardtii* photosystem I  
> Plant Physiology Aug 2018, pp.00749.2018  
> 
> DOI: [10.1104/pp.18.00749](https://doi.org/10.1104/pp.18.00749)  
> Pubmed ID: [30126869](https://www.ncbi.nlm.nih.gov/pubmed/30126869)  



# Contribute/Development
Contributions are welcome. 
After forking/cloning the git, for example via
```
git clone git@gitlab.com:bald/Crosslinx.git
```    
you need to initialize all submodules by
```
git submodule update --init --recursive
```

## Tests
Please make sure that all tests are passed when contributing new code. You can run the tests by executing the following code in the root directory of the repository
```
python3 tests/test.py "python3 crosslinx.py"
```

After pushing and merging the commit to gitlab, a continuous integration pipeline runs several tests and creates executables on different operating systems.

# References
Crosslinx uses several other tools.
* [Proteomatic](http://www.uni-muenster.de/hippler/proteomatic/)   
Specht, M., Kuhlgert, S., Fufezan, C. and Hippler, M. (2011)   
‘Proteomics to go: Proteomatic enables the user-friendly creation of versatile MS/MS data evaluation workflows.’,   
Bioinformatics, 27(8), pp. 1183–4. doi: [10.1093/bioinformatics/btr081](http://dx.doi.org/10.1093/bioinformatics/btr081).

* [pymzML](https://github.com/pymzml/pymzML)   
Bald, T., Barth, J., Niehues, A., Specht, M., Hippler, M. and Fufezan, C. (2012)   
‘pymzML - Python module for high-throughput bioinformatics on mass spectrometry data.’,   
Bioinformatics, 28(7), pp. 1052–3. doi: [10.1093/bioinformatics/bts066](http://dx.doi.org/10.1093/bioinformatics/bts066).

* [mpmath](http://mpmath.org/)    
Fredrik Johansson and others. (2011) 
mpmath: a Python library for arbitrary-precision floating-point arithmetic (version 0.17)   
[http://mpmath.org/](http://mpmath.org/).

* [Traveling Ruby](http://phusion.github.io/traveling-ruby/)
* [Ruby](https://www.ruby-lang.org)
* [pyinstaller](http://www.pyinstaller.org/)
* [pylint](https://www.pylint.org/)
* [coverage.py](https://coverage.readthedocs.io)


