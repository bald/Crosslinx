require './include/ruby/proteomatic'
require 'yaml'

# This is the hub to any other scripting language. Call with 
# [path to control file]

if ARGV.empty?
    puts "Usage: ruby any-language-hub.rb [control file]"
    exit(1)
end

controlFilePath = ARGV.shift
object = ProteomaticScript.new(nil, false, controlFilePath)
