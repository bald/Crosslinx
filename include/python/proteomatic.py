#!/usr/bin/env python
# encoding: utf-8
"""proteomatic.py class template 
"""

import sys
import os
from copy import copy
import subprocess
import tempfile
import json
import subprocess
import platform


class ProteomaticScript(object):
    """DocString to come
    """
    def run(self):
        raise NotImplementedError( "Should have implemented this" )
    
    def binaryPath(self,tool):
        try:
            return self.anyLanguageHubResponse['binaryPath'][tool]
        except KeyError:
            print("ERROR: The external tool {0} could not be found. Please check that the executable is in the right path.".format(tool))
            exit(1)
    
    def __init__(self, commands = copy(sys.argv)):
        scriptDir,scriptFilename = os.path.split(commands.pop(0))
        # crosslinx might be frozen (executable), 
        # get the directory where crosslinx.py/crosslinx executable is stored
        # the right directory is set in crosslinx.py
        scriptDir = os.getcwd()
        pathToRuby = "ruby"
        # detect the OS to decide on which ruby to use
        if platform.system() == "Windows":
            # use minimal ruby that is also shipped with proteomatic
            pathToRuby = "./helper/ruby/ruby-1.9.1-minimal-windows/bin/ruby"
        elif platform.system() == "Linux":
            # use traveling ruby
            # http://phusion.github.io/traveling-ruby
            pathToRuby = "./helper/ruby/traveling-ruby-20150715-2.2.2-linux-x86_64/bin/ruby"
        elif platform.system() == "Darwin":
            # use traveling ruby
            # http://phusion.github.io/traveling-ruby
            pathToRuby = "./helper/ruby/traveling-ruby-20150715-2.2.2-osx/bin/ruby"
        else:
            print("Unknown Operating System. Ruby may not work in this case.")
        if "--pathToRuby" in commands:
            pos = commands.index("--pathToRuby")
            commands.pop(pos)
            pathToRuby = commands.pop(pos)

        pathToYamlDescription = os.path.join(scriptDir,"include","properties","{0}.yaml".format(scriptFilename.split('.')[0]))
        
        controlFile  = tempfile.NamedTemporaryFile(mode='w',prefix='p-py-c-',delete=False)
        controlFilePath = controlFile.name
        #controlFile.write("action: query\npathToYamlDescription: \"{0}\"\nresponseFilePath: \"{1}\"\nresponseFormat: json\n".format(pathToYamlDescription,responseFilePath))
        controlFile.close()
        
        responseFile = tempfile.NamedTemporaryFile(mode='w',prefix='p-py-r-',delete=False)
        responseFilePath =responseFile.name
        responseFile.close()
        
        outputFile   = tempfile.NamedTemporaryFile(mode='w',prefix='p-py-o-',delete=False)
        outputFilePath= outputFile.name
        outputFile.close()
        
        argString = ''
        for cmd in commands:
            cmd = cmd.replace("\n", "\\n")
            cmd = cmd.replace("\t", "\\t")
            cmd = cmd.replace("\r", "\\r")
            cmd = cmd.replace("\"", "\\\"")
            argString += '  - "{0}"\n'.format(cmd)
            
        with open(controlFilePath,'w') as cf:
            cf.write('action: query\n')
            cf.write('pathToYamlDescription: "{0}"\n'.format(pathToYamlDescription.replace("\\","\\\\")))
            cf.write('responseFilePath: "{0}"\n'.format(responseFilePath.replace("\\","\\\\")))
            cf.write('responseFormat: json\n')
            cf.write('arguments:\n{0}'.format(argString))
        
        hubPath = os.path.normpath(os.path.join(scriptDir,'helper','any-language-hub.rb'))
        subprocess.call([pathToRuby, hubPath, controlFilePath], cwd = os.getcwd())

        responseFile = open(responseFilePath)
        self.anyLanguageHubResponse = {}
        try:
            self.anyLanguageHubResponse = json.load(responseFile)           
        except:
            pass
        if 'run' in self.anyLanguageHubResponse.keys():
            if self.anyLanguageHubResponse['run'] == 'run':
                # removed 0 to make it Python 3 compatible
                outputFile = open(outputFilePath, 'w')#, 0)
                # removed the following line to work with multiple tests from unittest
                #sys.stdout = os.fdopen(sys.stdout.fileno(), 'w')#, 0)
                os.dup2(sys.stdout.fileno(),outputFile.fileno())
            
                #sys.stdout = outputFile
                self.param  = self.anyLanguageHubResponse['param']
                self.input  = self.anyLanguageHubResponse['input']
                self.output = self.anyLanguageHubResponse['output']

                # make sure, that there are no old temp output files
                for _outputKey, _outputFilePath in self.output.items():
                    try:
                        os.remove(_outputFilePath)
                    except OSError:
                        # the file does not exist. this is good and we can go on
                        pass

                self.run()
                outputFile.close()
            
                with open(controlFilePath,'w') as cf:
                    cf.write('action: finish\n')
                    cf.write('pathToYamlDescription: "{0}"\n'.format(pathToYamlDescription.replace("\\","\\\\")))
                    cf.write('responseFilePath: "{0}"\n'.format(responseFilePath.replace("\\","\\\\")))
                    cf.write('responseFormat: json\n')
                    cf.write('arguments:\n{0}'.format(argString))
                    cf.write('outputFilePath: "{0}"\n'.format(outputFilePath.replace("\\","\\\\")))
                    cf.write('startTime: {0}\n'.format(self.anyLanguageHubResponse['startTime']))
                
                subprocess.call([pathToRuby, hubPath, controlFilePath])
        else:
            pass
        
        responseFile.close()
        os.unlink(controlFilePath)
        os.unlink(responseFilePath)
        os.unlink(outputFilePath)

def main():
    pass


if __name__ == '__main__':
    main()
