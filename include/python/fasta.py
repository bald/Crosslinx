# Copyright (c) 2011-2017 Michael Specht and Till Bald
# 
# This file is part of Proteomatic.
# 
# Proteomatic is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Proteomatic is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Proteomatic.  If not, see <http://www.gnu.org/licenses/>.


def parseFasta(io, NTerminus = False):
    '''this is a generator which spits out a (id, sequence) tuple for
    every FASTA entry, no checks are performed but newlines are removed'''
    id = None
    sequence = ''
    for line in io:
        line = line.strip()
        if line and line[0] == '>':
            if id:
                yield(id, sequence)
            id = line[1::]
            if NTerminus:
                sequence = '_'
            else:
                sequence = '' 
        else:
            sequence += line
    if id:
        yield(id, sequence)

def getProteinID(peptide, fasta_iter):
    # use fasta_iter from fasta.parseFasta
    resulting_ids = set()
    for item in fasta_iter:
        if not item[1].find(peptide) == -1:
            resulting_ids.add(item[0])
    return sorted(resulting_ids)

def digestPeptides(sequence, missedCleavageSites, enzyme, notCleavedAminoAcidsString = ""):
    '''returns a set of tryptic peptides
    enzyme:      enzyme string from proteomatic parameters as follows: someEnyzmeName (AK|)
    missedCleavageSites:    integer
    notCleavedAminoAcidsString: string from proteomatic parameters as follows: someEnyzmeName (P) or empty string
    '''
    
    start = enzyme.index("(")+1
    stop = enzyme.index("|)")
    cleavedAminoAcids = enzyme[start:stop:]

    notCleavedAminoAcids = ""
    if not len(notCleavedAminoAcidsString) == 0:
        start = notCleavedAminoAcidsString.index("(")+1
        stop = notCleavedAminoAcidsString.index(")")
        notCleavedAminoAcids =  notCleavedAminoAcidsString[start:stop:]
    
    result = set()
    if sequence[-1] == '*':
        sequence = sequence[0:-1]
    indices = [-1]
    for i in range(len(sequence)):
        if sequence[i] in cleavedAminoAcids:
            if notCleavedAminoAcids:
                if sequence[i-1] and not sequence[i-1] in notCleavedAminoAcids:
                    indices.append(i)
            else:
                indices.append(i)
    indices.append(len(sequence))
    for i in range(len(indices)):
        maxk = missedCleavageSites + 1
        if i + maxk > len(indices) - 1:
            maxk = len(indices) - 1 - i
        for k in range(maxk):
            start = indices[i] + 1
            stop = indices[i + k + 1] + 1
            result.add(sequence[start:stop])
    # NOTE somewhere is an empty peptide sequence
    # probably on sequences which end on the cleavage site
    # remove empty strings
    try:
        result.remove("")
    except KeyError:
        pass
    
    return result

def parseFastaAndDigest(io, missedCleavageSites, cleavedAminoAcids, notCleavedAminoAcids, NTerminus = False):
    allPeptides = set()
    #if io.endswith('.gz'):
        #import gzip, codecs
        #io = codecs.getreader("utf-8")(gzip.open(io))
    for item in parseFasta(open(io), NTerminus):
        id = item[0]
        sequence = item[1]
        allPeptides |= digestPeptides(sequence, missedCleavageSites, cleavedAminoAcids, notCleavedAminoAcids)
    return allPeptides
    
def reversePeptide(peptide):
    if peptide[0] == "_":
        peptide = peptide[1:]
    return peptide[:-1][::-1]+peptide[-1]
