# Copyright (c) 2011-2013 Till Bald
# 
# This file is part of Proteomatic.
# 
# Proteomatic is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Proteomatic is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Proteomatic.  If not, see <http://www.gnu.org/licenses/>.



# import this script in any proteomatic python scripts as follows:
# import proteomicsKnowledge
#
# acess variables:
# proteomicsKnowledge.PROTON_MASS

from __future__ import print_function
import csv
from copy import deepcopy
from collections import defaultdict as defaultdict
import re


PROTON_MASS = 1.00727646677

proteomicsKnowledge = dict()



reader = csv.reader(open("include/proteomics-knowledge-base/amino-acids.csv", "r"))
proteomicsKnowledge["aminoacids"] = dict()
proteomicsKnowledge['protonMass_internal'] = dict()
header = next(reader)
for lineArray in reader:
    singlelettercode = lineArray[header.index("single letter code")]
    proteomicsKnowledge["aminoacids"][singlelettercode] = dict()
    proteomicsKnowledge["aminoacids"][singlelettercode]["id"] = lineArray[header.index("#id")]
    proteomicsKnowledge["aminoacids"][singlelettercode]["fullname"] = lineArray[header.index("full name")]
    proteomicsKnowledge["aminoacids"][singlelettercode]["threelettercode"] = lineArray[header.index("three letter code")]
    proteomicsKnowledge["aminoacids"][singlelettercode]["singlelettercode"] = lineArray[header.index("single letter code")]
    proteomicsKnowledge["aminoacids"][singlelettercode]["monoisotopicmass"] = float(lineArray[header.index("monoisotopic mass")])
    proteomicsKnowledge["aminoacids"][singlelettercode]["monoisotopicmass_internal"] = dict()
    proteomicsKnowledge["aminoacids"][singlelettercode]["averagemass"] = float(lineArray[header.index("average mass")])
    proteomicsKnowledge["aminoacids"][singlelettercode]["composition"] = lineArray[header.index("composition")]
    

    

reader = csv.reader(open("include/proteomics-knowledge-base/isotopes.csv", "r"))
proteomicsKnowledge["isotopes"] = dict()
header = next(reader)
for lineArray in reader:
    element = lineArray[header.index("Element")]
    isotope = int(lineArray[header.index("Isotope")])
    monoisotopicmass = lineArray[header.index("Monoisotopic mass")]
    abundance = lineArray[header.index("Natural abundance")]
    if not element in proteomicsKnowledge["isotopes"].keys():
        proteomicsKnowledge["isotopes"][element] = dict()
    proteomicsKnowledge["isotopes"][element][isotope] = dict()
    proteomicsKnowledge["isotopes"][element][isotope]["monoisotopicmass"] = float(monoisotopicmass)
    proteomicsKnowledge["isotopes"][element][isotope]["monoisotopicmass_internal"] = dict()
    
    proteomicsKnowledge["isotopes"][element][isotope]["abundance"] = float(abundance)

for key in proteomicsKnowledge["isotopes"]:
    lightestIsotope = sorted(proteomicsKnowledge["isotopes"][key].keys())[0]
    proteomicsKnowledge["isotopes"][key]["default"] = deepcopy(proteomicsKnowledge["isotopes"][key][lightestIsotope])

# compile regex pattern for amino acid composition
pattern = re.compile(r'[A-Z]{1}\d*')

for singlelettercode in proteomicsKnowledge["aminoacids"].keys():
    aa_composition_string = proteomicsKnowledge["aminoacids"][singlelettercode]["composition"]
    aa_composition_list = re.findall(pattern, aa_composition_string)
    # aa_composition_list = ['C9', 'H9', 'N', 'O']

    proteomicsKnowledge["aminoacids"][singlelettercode]["composition_dict"] = defaultdict(int)

    for part in aa_composition_list:
        if len(part) == 1:
            # this element occurs once
            proteomicsKnowledge["aminoacids"][singlelettercode]["composition_dict"][part] += 1
        else:
            # get element and count
            element  = part[0]
            count = int(part[1:])
            proteomicsKnowledge["aminoacids"][singlelettercode]["composition_dict"][element] += count
    







def elementMass(element):
    return proteomicsKnowledge["isotopes"][element]["default"]["monoisotopicmass"]




def aminoAcidMass(aa, label = None):
    answer = None
    if label == None:
        answer = proteomicsKnowledge["aminoacids"][aa]["monoisotopicmass"]
    else:
        # label = '15N' or 13C, ...
        answer = 0
        labelled_element = label[-1]
        isotope_number = int(label[:-1])
        for element, element_count in proteomicsKnowledge["aminoacids"][aa]["composition_dict"].items():
            if element != labelled_element:
                answer += proteomicsKnowledge["isotopes"][element]["default"]["monoisotopicmass"]
            else:
                answer += proteomicsKnowledge["isotopes"][element][isotope_number]["monoisotopicmass"]

    return answer
        
    
    
    
def peptideMass(peptide, label = None):
    mass = elementMass('H') * 2.0 + elementMass('O')
    try:
        for aa in peptide:
            mass += aminoAcidMass(aa, label)
    except KeyError:
        print('Error during mass calculations for peptide {}.'.format(peptide))
        exit(1)
    return mass
    



def elementMass_internalPrecision(element, internalPrecision):
    try:
        return proteomicsKnowledge["isotopes"][element]["default"]["monoisotopicmass_internal"][internalPrecision]
    except KeyError:
        calculateDict(internalPrecision)
        return proteomicsKnowledge["isotopes"][element]["default"]["monoisotopicmass_internal"][internalPrecision]

def aminoAcidMass_internalPrecision(aa, internalPrecision):
    try:
        return proteomicsKnowledge["aminoacids"][aa]["monoisotopicmass_internal"][internalPrecision]
    except KeyError:
        calculateDict(internalPrecision)
        return proteomicsKnowledge["aminoacids"][aa]["monoisotopicmass_internal"][internalPrecision]
    
def peptideMass_internalPrecision(peptide, internalPrecision, flag_18O = False):
    mass = elementMass_internalPrecision('H', internalPrecision) * 2 + elementMass_internalPrecision('O', internalPrecision)
    try:
        for aa in peptide:
            mass += aminoAcidMass_internalPrecision(aa, internalPrecision)
    except KeyError:
        print('Error during mass calculations for peptide {}. This peptide will be skipped.'.format(peptide))
        pass
    if flag_18O == True:
        mass += int(round(2.00424578 * internalPrecision))
    
    return mass


def calculateDict(internalPrecision):
    for singlelettercode in proteomicsKnowledge["aminoacids"].keys():
        proteomicsKnowledge["aminoacids"][singlelettercode]["monoisotopicmass_internal"][internalPrecision] = int(round(proteomicsKnowledge["aminoacids"][singlelettercode]["monoisotopicmass"] * internalPrecision))
    
    for element in proteomicsKnowledge["isotopes"]:
        for isotope in proteomicsKnowledge["isotopes"][element]:
            proteomicsKnowledge["isotopes"][element][isotope]["monoisotopicmass_internal"][internalPrecision] = int(round(proteomicsKnowledge["isotopes"][element][isotope]["monoisotopicmass"] * internalPrecision))
    
    proteomicsKnowledge['protonMass_internal'][internalPrecision] = int(round(PROTON_MASS * internalPrecision))
    

# The ruby code below should be implented in python


#File::open('include/proteomics-knowledge-base/genetic-codes.txt', 'r') do |f|
    #goodLines = []
    #f.each_line do |line|
        #line.strip!
        #next if line.empty? || line[0, 1] == '#'
        #goodLines << line
    #end
    #$proteomicsKnowledge[:geneticCodes] = Hash.new
    #$proteomicsKnowledge[:geneticCodes][1] = Hash.new
    #goodLines.delete_at(0)
    #(0..4).each do |i|
        #goodLines[i] = goodLines[i][-64, 64]
    #end
    #(0...64).each do |i|
        #triplet = ''
        #(0...3).each do |k|
            #triplet += goodLines[2 + k][i, 1]
        #end
        #$proteomicsKnowledge[:geneticCodes][1][triplet] = goodLines[0][i, 1]
    #end
#end


#def translateDna(dna)
    #result = ''
    #i = 0
    #while i + 2 < dna.length 
        #triplet = dna[i, 3]
        #result += $proteomicsKnowledge[:geneticCodes][1][triplet]
        #i += 3
    #end
    #return result
#end
