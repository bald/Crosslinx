# Copyright (c) 2011-2012 Till Bald
# 
# This file is part of Proteomatic.
# 
# Proteomatic is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Proteomatic is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Proteomatic.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function
import sys, os
sys.path.append('./include/python')
import proteomatic
import proteomicsKnowledge
import math
try:
    from mpmath import *
    mp.dps = 200
except ImportError:
    print("Import Error while importing mpmath. As long as E value calculation is not needed, this doesnt matter.")

ISOTOPE_AVERAGE_DIFFERENCE = 1.002
PROTON_MASS = proteomicsKnowledge.PROTON_MASS
mgf_retention_time = dict()


def ppm2abs(value, ppmValue, direction = 1, factor = 1):
    '''
    returns the value plus (or minus, dependent on direction) the imprecession for this value
    direction should be 1 or -1
    factor should be bigger than 0
    '''
    result = value + ppm(value, factor, ppmValue)*direction
    return result        
            
def ppm(value, factor, ppmValue):
    '''
    Return the imprecession for a given value
    '''
    
    result = value*(ppmValue*factor)/1.0e6
    return result
    
    
def chargeDeconvolution(mz_tuple, intensity, minCharge, maxCharge, ppmValue, ppmFactor):
    '''
    Find isotope envelopes in given spectrum (with m/z values), calculate the charge and return a spectrum containing monoisotopic uncharged masses (not mz).
    mz_tuple should be a sorted list or tuple (in order of ascending mz)
    intensity should contain the corresponding intensities
    minCharge and maxCharge give the range, in which isotope envelopes are searched
    ppmValue gives the imprecession for searching isotope envelope peaks
    ppmFactor is a factor for the former ppmValue, which is used for the absence of a peak before the currently analysed monoisotopic peak
    
    Returns a sorted nested list with masses and intensities. Masses are grouped together (for details see function group)
    '''
    
    # calculate monoisotopic m/z and charge
    interestingPeaks = monoisotopicMZ(mz_tuple, intensity, minCharge, maxCharge, ppmValue, ppmFactor)
    
    # charge deconvolution
    result = []
    for triplet in interestingPeaks:
        mz, intensity, charge = triplet
        mass = mz2mass(mz, charge)
        result.append(tuple([mass, intensity]))
    
    # sort the result corresponding to the mass (due to the mz to mass conversion, the values are no longer sorted)
    result = sorted(result)
    
    # check on empty result list
    if len(result) == 0:
        # No peaks could be identified for charge deconvolution.
        return []
    
    # group peaks
    result = group(result, ppmValue)
            
    return result
    
def monoisotopicMZ(mz_tuple, intensity, minCharge, maxCharge, ppmValue, ppmFactor):
    '''
    '''
    
    # check input vars
    if not len(mz_tuple) == len(intensity):
        print('Error: Wrong input for function. mz_tuple and intensity should have the same length.')
        exit(1)
    
    interestingPeaks = []
    for i in range(len(mz_tuple)):
        for charge in range(maxCharge, minCharge - 1, -1):
            # check absence of isotope envelope peaks before the current peak
            j = i - 1
            target = mz_tuple[i] - ISOTOPE_AVERAGE_DIFFERENCE / charge
            target_min = ppm2abs(target, ppmValue, -1, ppmFactor)
            target_max = ppm2abs(target, ppmValue, 1, ppmFactor)
            found = False
            while j >= 0 and mz_tuple[j] >= target_min:
                if mz_tuple[j] <= target_max:
                    found = True
                    break
                j = j - 1
            
            # if a potential precursor peak for the current peak is found, jump to the next peak
            if found:
                continue
            
            # check presence of isotope envelope after the current peak
            # this does not consider intensities of the peaks
            j = i+1
            target = mz_tuple[i] + ISOTOPE_AVERAGE_DIFFERENCE / charge
            target_min = ppm2abs(target, ppmValue, -1, 1)
            target_max = ppm2abs(target, ppmValue, 1, 1)
            found = False
            intensity_sum = intensity[i]
            while j < len(mz_tuple) and mz_tuple[j] <= target_max:
                if mz_tuple[j] >= target_min:
                    found = True
                    intensity_sum += intensity[j]
                    # go to next j and reset the target
                    j += 1
                    if not j >= len(mz_tuple):
                        target = mz_tuple[j] + ISOTOPE_AVERAGE_DIFFERENCE / charge
                        target_min = ppm2abs(target, ppmValue, -1, 1)
                        target_max = ppm2abs(target, ppmValue, 1, 1)
                else:
                    j += 1
                    # this does only consider, that one peak lies within the expected distance, two or more are not considered.
            
            if found:
                interestingPeaks.append(tuple([mz_tuple[i], intensity_sum, charge]))
                # as the first peak of the  isotope envelope is added here, this is a monoisotopic peak.
    return interestingPeaks
    
def mz2mass(mz, charge):
    '''
    Calculate the uncharged mass for a given mz value
    '''
    return ((mz - PROTON_MASS) * charge)

def group(data, ppmValue = 5.0, intensity_tuple = False):
    '''
    Group mz (or mass) values according to the given ppm value. The mean value of grouped peaks is stored. 
    When an intensity tuple is given, the corresponding intensity are summed up and stored.
    Input possibilities
    a) data: only mz values, no intensity_tuple given
    b) data: mz values and intensities, no intensity_tuple given
    c) data: only mz values, intensity_tuple: intensities
    In cases b and c, the intensities are summed up for grouped peaks.
    
    Returns a tuple containing either only mz values or tuples of mz values and intensities if intensities were given in the input variable.
    '''
    
    # check for intensities
    sum_up_intensities = False
    if len(data[0]) == 2:
        # data contains mz values and intensities, unsplit them
        # as intensities are given, they should be summed up in later steps
        sum_up_intensities = True
        mz_tuple, intensity_tuple = zip(*data)
    elif len(data[0]) == 1:
        # data contains only mz values
        mz_tuple = data
        if intensity_tuple:
            # as intensities are given, they should be summed up in later steps
            sum_up_intensities = True
    else:
        # Error handling
        print('No valid input in function "group".')
        exit(1)
    
    count_ungrouped = 0
    mz_list_grouped = []
    i = 0
    # iterate over all entries for grouping
    while i < len(mz_tuple):
        target =  ppm2abs(mz_tuple[i], ppmValue, 1, 1)
        j = i + 1
        while j < len(mz_tuple) and mz_tuple[j] <= target:
            j += 1
        j = j- 1
        if i == j:
            # no peaks have to be grouped, just add the current peak to the result and go in with the next peak
            if sum_up_intensities:
                mz_list_grouped.append(tuple([mz_tuple[i], intensity_tuple[i]]))
                #print([mz_tuple[i], intensity_tuple[i]])
            else:
                mz_list_grouped.append(mz_tuple[i])
            i += 1
        else:
            # potential overlapping peaks are found.
            # check wether the mz value of the j index does not overlap with the next j+1 index
            k = j + 1
            group = True
            if k < len(mz_tuple):
                target_new = ppm2abs(mz_tuple[j], ppmValue, 1, 1)
                if target_new >= mz_tuple[k]:
                    group = False
            
            if group:
                # group the peaks, calculate mean
                mean = sum(mz_tuple[i:j+1])/len(mz_tuple[i:j+1])
                if sum_up_intensities:
                    intensity_sum = sum(intensity_tuple[i:j+1]) 
                    mz_list_grouped.append(tuple([mean, intensity_sum]))
                    #print([mean, intensity_sum])
                    #print('i', i, 'j', j)
                else:
                    mz_list_grouped.append(mean)
                i = j + 1
            else:
                # the are ambigious, no grouping is applied --> every peak is stored
                # this incident is counted.
                count_ungrouped += j - i
                # adding each element between i and j
                if sum_up_intensities:
                    for k in range(i, j + 1):
                        mz_list_grouped.append(tuple([mz_list[k], intensity_tuple[k]]))
                else:
                    for k in range(i, j + 1):
                        mz_list_grouped.append(mz_list[k])                        
                i = j + 1
    
    if count_ungrouped:
        # if ungrouped entries occured, this is reported
        print('{} elements could not be grouped due to an overlap.'.format(count_ungrouped))
    return tuple(mz_list_grouped)

def relativeIntensities(intensities):
    '''
    Calculate relative intensities.
    Input: list or tuple of intensities
    Output: list with relative intensities (the order of the input list is maintained)
    '''
    max_intensity = max(intensities)
    result = []
    for intensity_value in intensities:
        result.append(intensity_value/max_intensity*100.0)
    return result

def noiseReduction(mz, intensities, treshold):
    '''
    Noise reduction: returns only those pairs, which have an intensity above the specified treshold.
    Input: lists or tuples for mz and intensities, relative (in %) or absolute treshold (depending on the given intensities)
    Output: lists containing the remaining data
    '''
    # if the treshold is 0, do no adjustments, just return input values
    if treshold == 0:
        return mz, intensities
    # do the noise reduction
    else:
        mz_result = []
        intensities_result = []
        for i in range(len(mz)):
            if intensities[i] >= treshold:
                mz_result.append(mz[i])
                intensities_result.append(intensities[i])
        return mz_result, intensities_result

def eValue_old(numberOfSuccessfullProductIonMatches, tolerance, totalNumberOfCalculatedValues, numberOfExperimentalValues, precursorMass, numberOfTheoreticalSpectra, numberOfTopPeaks = 3):
    # numberOfSuccessfullProductIonMatches  y
    # tolerance                             t
    # totalNumberOfCalculatedValues         h
    # numberOfExperimentalValues            v
    # precursorMass                         m
    # numberOfTheoreticalSpectra            N
    # numberOfTopPeaks = 3                  n
    
    mu = 2 * tolerance * totalNumberOfCalculatedValues * numberOfExperimentalValues / precursorMass
    print('mu', mu)
    q = numberOfTopPeaks / numberOfExperimentalValues
    print('q', q)
    sum = 0
    #normalizationFactor = normalizationFactorFunction(numberOfSuccessfullProductIonMatches, numberOfTopPeaks, numberOfExperimentalValues, q, mu)
    #print('normalizationFactor', normalizationFactor)
    #if normalizationFactor == 0:
        #return 10000
    for x in range(1, numberOfSuccessfullProductIonMatches + 1):
        normalizationFactor = normalizationFactorFunction(x, numberOfTopPeaks, numberOfExperimentalValues, q, mu)
        sum += pDashFunction(x, mu, normalizationFactor, q)
        #print(sum)
    print('sum evalue',  sum)
    eValue = numberOfTheoreticalSpectra * (1 - sum**numberOfTheoreticalSpectra)
    print('eValue', eValue)
    return eValue

def eValue_new(numberOfSuccessfullProductIonMatches, tolerance, totalNumberOfCalculatedValues, numberOfExperimentalValues, precursorMass, numberOfTheoreticalSpectra, numberOfTopPeaks = 3):
    # numberOfSuccessfullProductIonMatches  y
    # tolerance                             t
    # totalNumberOfCalculatedValues         h
    # numberOfExperimentalValues            v
    # precursorMass                         m
    # numberOfTheoreticalSpectra            N
    # numberOfTopPeaks = 3                  n
    
    mu = 2 * tolerance * totalNumberOfCalculatedValues * numberOfExperimentalValues / precursorMass
    q = numberOfTopPeaks / numberOfExperimentalValues
    
    nenner_summe = 0
    sum_result = 0
    
    for x in range(1, numberOfSuccessfullProductIonMatches):
        pDash_counter = ((1 - ((1 - q) ** x)) * pFunction(x, mu))
        nenner_summe += pDash_counter
        sum_counter = (pDash_counter/nenner_summe)
        sum_result += sum_counter
    
    eValue = numberOfTheoreticalSpectra * (1 - (sum_result**numberOfTheoreticalSpectra))
    return eValue

def eValue_test2(numberOfSuccessfullProductIonMatches, tolerance, totalNumberOfCalculatedValues, numberOfExperimentalValues, precursorMass, numberOfTheoreticalSpectra, numberOfTopPeaks = 3):
    # numberOfSuccessfullProductIonMatches  y
    # tolerance                             t
    # totalNumberOfCalculatedValues         h
    # numberOfExperimentalValues            v
    # precursorMass                         m
    # numberOfTheoreticalSpectra            N
    # numberOfTopPeaks = 3                  n
    
    mu = 2 * tolerance * totalNumberOfCalculatedValues * numberOfExperimentalValues / precursorMass
    q = numberOfTopPeaks / numberOfExperimentalValues
    
    sum_result = 0
    pDash_counter_sum = 0
    for x in range(1, numberOfSuccessfullProductIonMatches + 1):
        pDash_counter_sum += ((1 - ((1 - q) ** x)) * pFunction(x, mu))
    
    for x in range(1, numberOfSuccessfullProductIonMatches):
        pDash_counter = ((1 - ((1 - q) ** x)) * pFunction(x, mu))
        sum_result += pDash_counter
    
    print(sum_result, pDash_counter_sum)
    #print(pFunction(numberOfSuccessfullProductIonMatches, mu))
    eValue = numberOfTheoreticalSpectra* (1 - ((sum_result/pDash_counter_sum)**numberOfTheoreticalSpectra))
    return eValue

def eValue(numberOfSuccessfullProductIonMatches, tolerance, totalNumberOfCalculatedValues, numberOfExperimentalValues, precursorMass, numberOfTheoreticalSpectra, numberOfTopPeaks = 3):
    # numberOfSuccessfullProductIonMatches  y
    # tolerance                             t
    # totalNumberOfCalculatedValues         h
    # numberOfExperimentalValues            v
    # precursorMass                         m
    # numberOfTheoreticalSpectra            N
    # numberOfTopPeaks = 3                  n
    
    tolerance = mpf(str(tolerance))
    precursorMass = mpf(str(precursorMass))
    precursorMass = mpf(str(precursorMass))
    
    mu = 2 * tolerance * totalNumberOfCalculatedValues * numberOfExperimentalValues / precursorMass
    q = numberOfTopPeaks / numberOfExperimentalValues
    
    sum_result = 0
    pDash_counter_sum = 0
    pDash_counter = 0.0
    for x in range(1, numberOfSuccessfullProductIonMatches):
        pDash_counter += ((1 - ((1 - q) ** x)) * pFunction(x, mu))
    last_summand = ((1 - ((1 - q) ** numberOfSuccessfullProductIonMatches)) * pFunction(numberOfSuccessfullProductIonMatches, mu))
    
    eValue = numberOfTheoreticalSpectra* (1 - ((pDash_counter/(pDash_counter+last_summand))**numberOfTheoreticalSpectra))
    return float('%1.6g' % mpf(eValue))

def normalizationFactorFunction(x, numberOfTopPeaks, numberOfExperimentalValues, q, mu):
    # Q
    result = 0
    for i in range(1, x + 1):
        result += (1 - (1 - q) ** i) * pFunction(i, mu)
    return result

def pFunction(x, mu):
    #mp.dps = 100
    return (mpf(mu) ** x)/ math.factorial(x) * math.exp(-mu)

def pFunction_(x, mu):
    return (round(mu) ** x)/ math.factorial(x) * math.exp(-mu)

def pDashFunction(x, mu, normalizationFactor, q):
    return 1 / normalizationFactor * (1 - (1 - q) ** x) * pFunction(x, mu)
    
def mzml2mgf(mzmlFile,                      # path to input mzml file
             outputFile,                    # path to output mgf file
             ms_levels = [2],               # ms levels that should be considered, examples: [1, 2] or [2]
             bioworks_title = False,        # write mgf title line for each spectrum in bioworks style (includes the whole path)
             ppmMS1 = 20.0,                 # set mass accuracy, needed for deconvolution mainly
             ppmMS2 = 700.0,                # set mass accuracy, needed for deconvolution mainly
             write_retention_time = False,  # write retention time in an extra line and in the title line
             write_scans = False,           # write extra line with scan number
             removeNoise = "No",            # remove noise (pymzml spec function), possible values "No", "mad", "median", "mean"
             deconvolution = False,         # deconvolute spectra
             max_charge = 8,                # maximum charge that is considered in deconvolution
             mzDecimals = 4,                # number of decimals for mz values
             intensityDecimals = 4,         # number of decimals for intensity values
             rtDecimals = 4):               # number of decimals for retention time values
    
    import pymzml
    
    # read mzMlFiles and store data
    print('Converting mzML to mgf ...', end = '\r')
    sys.stdout.flush()
    
    # open output file
    mgf_out = open(outputFile, 'w', newline = '')
    
    # set title to either filename or the whole path
    if bioworks_title:
        title_file = mzmlFile
    else:
        # get file basename only
        title_file = mgf_filename(mzmlFile)


    # read all spectra
    run = pymzml.run.Reader(mzmlFile, MS1_Precision = ppmMS1 * 1e-6, MSn_Precision = ppmMS2 * 1e-6)
    for s in run:
        if s['ms level'] in ms_levels:
            # get retention time
            #retention_time_prefix = ""
            #retention_time        = ""
            if write_retention_time:
                #retention_time_prefix = ".RT=" 
                retention_time = s['scan time']
            
            if s['ms level'] > 1 and len(s['precursors']) == 1:
                # set title for MSn scans
                mz = s['precursors'][0]['mz']
                charge = "{0}".format(s['precursors'][0]['charge'])
                title = mgf_title(title_file, s['id'], charge)
            elif  s['ms level'] > 1 and len(s['precursors']) != 1:
                # skip MSn scans with multiple precursors
                continue
            elif s['ms level'] == 1:
                # set title for MS1 scans
                title = '{0}.{1}.MS1'.format(title_file, s['id'])
                charge = 'NA'
                mz = 'NA'
            
            # add retention_time to title (if parameter retention time is false, the string is empty)
            #title = "{0}{1}{2}\r\n".format(title, retention_time_prefix, retention_time)
            
            
            # remove noise?
            if removeNoise != 'No':
                s.removeNoise(removeNoise)
            
            # deconvolution?
            if deconvolution:
                #peaks_to_write = s.deconvolutedPeaks
                peaks_to_write = s.deconvolute_peaks(ppmFactor = 4, minCharge = 1, maxCharge = self.param['maxCharge'], maxNextPeaks = 100)
            else:
                peaks_to_write = s.centroidedPeaks
                
            if len(peaks_to_write) == 0:
                continue
            
            # write data
            rt_minutes = ''
            rt_seconds = ''
            if write_retention_time:
                rt_seconds = retention_time * 60
                rt_minutes = retention_time
            scans = ''
            if write_scans == True:
                scans =s['id']
            mgf_writer(mgf_out, title, charge, mz, scans, rt_minutes, rt_seconds, peaks_to_write)

    print('Converting mzML to mgf done.')
    sys.stdout.flush()
    
    return outputFile

def mgf_title(filename, spectrumID, charge):
    # format title
    charge = "{0}".format(charge)
    title = '{0}.{1}.{1}.{2}'.format(filename, spectrumID, charge)
    return title

def mgf_filename(mzmlFile):
    # get file basename only
    split_on = ''
    if '.mzml' in mzmlFile:
        split_on = '.mzml'
    elif '.mzML' in mzmlFile:
        split_on = '.mzML'
    else:
        print('File extension for "{}" not known.'.format(mzmlFile))
        print('File extension has to be either ".mzml" or ".mzML" (upper/lowercase is important here). Compressed formats are also possible, for example ".mzML.gz". The file extension for the compression does not need to be in a specific format.')

    title_file = os.path.basename(mzmlFile).split(split_on)[0]
    return title_file

def mgf_writer(mgf_out_io, title, charge, precursor_mz, scans, rt_minutes, rt_seconds, mz_i_tuples, rtDecimals = 4, mzDecimals = 4, intensityDecimals = 4):
        # write data
        mgf_out_io.write('BEGIN IONS\r\n')
        mgf_out_io.write('TITLE={0}\r\n'.format(title))
        mgf_out_io.write('CHARGE={0}\r\n'.format(charge))
        mgf_out_io.write('PEPMASS={0}\r\n'.format(precursor_mz))
        if scans:
            mgf_out_io.write('SCANS={0}\r\n'.format(scans))

        # at this point, we do not know the dimension of the retention time
        # most probably it is minutes
        if rt_seconds:
            mgf_out_io.write('RTINSECONDS={0}\r\n'.format(('%.0{0}f'.format(rtDecimals) % rt_seconds)))
        if rt_minutes:
            mgf_out_io.write('RT={0}\r\n'.format(('%.0{0}f'.format(rtDecimals) % rt_minutes)))

        # write ions
        for mz, i in mz_i_tuples:
            # in the case of deconvolution, mz does not contain mz values but masses ...
            mgf_out_io.write('{0} {1}\r\n'.format(('%.0{0}f'.format(mzDecimals) % mz), ('%.0{0}f'.format(intensityDecimals) % i)))
        mgf_out_io.write('\r\n')
        mgf_out_io.write('END IONS\r\n')
        mgf_out_io.write('\r\n')

        return True

def get_retention_time(mgf_input, title):
    title = title.strip()
    try:
        mgf_retention_time[mgf_input]
    except KeyError:
        print("Reading retention times from mgf file.")
        # mgf file was not read in, do this now
        mgf_retention_time[mgf_input] = dict()
        
        # read retention time from file
        last_title  = ''
        for line in open(mgf_input, 'r'):
            if line.startswith('TITLE='):
                last_title = line.split('TITLE=')[-1].strip()
            elif line.startswith('RT='):
                retention_time = line.split('RT=')[-1].strip()
                mgf_retention_time[mgf_input][last_title] = retention_time
        # read retention time from file done!
    
    try:
        retention_time = mgf_retention_time[mgf_input][title]
    except KeyError:
        print("Spectrum with title '{0}' or retention time for that spectrum could not be found in mgf file '{1}'.".format(title, mgf_input))
        retention_time = ""

    return retention_time

def mgf_reader(mgf_input):
    # read mgf
    spectrum = dict()
    for line in open(mgf_input, 'r'):
        if line.strip() == '':
            # pass empty line
            continue
        if line.strip() == 'BEGIN IONS':
            # new spectrum
            if spectrum:
                print("ERROR in mgf file. A new spectrum started (BEGIN IONS), but the old spectrum did not end (END IONS)!")
                exit(1)
            # initialize peaks
            spectrum['peaks'] = []
        elif line.startswith('TITLE='):
            spectrum['title'] = line.split('TITLE=')[-1].strip()
        elif line.startswith('RT='):
            spectrum['retention time'] = float(line.split('RT=')[-1].strip())
        elif line.startswith('CHARGE='):
            spectrum['charge'] = int(line.split('CHARGE=')[-1].strip())
        elif line.startswith('PEPMASS='):
            spectrum['pepmass'] = float(line.split('PEPMASS=')[-1].strip())
        elif line.startswith('SCANS='):
            spectrum['scans'] = line.split('SCANS=')[-1].strip()
        elif line.startswith('RTINSECONDS='):
            spectrum['rtinseconds'] = float(line.split('RTINSECONDS=')[-1].strip())
        elif line.strip() == 'END IONS':
            yield spectrum
            spectrum = dict()
        else:
            # these should be the peaks
            spectrum['peaks'].append([float(line.strip().split()[0]), float(line.strip().split()[1])])
