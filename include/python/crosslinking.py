'''
Copyright (c) 2011-2019 Till Bald

Crosslinx is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

Crosslinx is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Crosslinx.  If not, see <http://www.gnu.org/licenses/>.
'''

from __future__ import print_function
import sys
from collections import defaultdict
from collections import OrderedDict
from bisect import bisect_left as bisect_left

sys.path.append('./include/python')
import fasta
import proteomicsKnowledge

CARBOXAMIDOMETHYL_SHIFT = 57.02146
ELEMENT_MASS_H = proteomicsKnowledge.elementMass('H')
ELEMENT_MASS_N = proteomicsKnowledge.elementMass('N')
ELEMENT_MASS_C = proteomicsKnowledge.elementMass('C')
ELEMENT_MASS_O = proteomicsKnowledge.elementMass('O')
ELEMENT_MASS_INTERNAL_PRECISION = proteomicsKnowledge.elementMass_internalPrecision
proton_mass_internal_precision = proteomicsKnowledge.proteomicsKnowledge[
    'protonMass_internal']
fragment_ion_cache = dict()
fragment_peptide_cache = OrderedDict()


def fragmentPeptide(peptide):
    try:
        # use item from cache
        result = fragment_peptide_cache[peptide]
    except KeyError:
        # calculate
        result = set()

        for index in range(0, len(peptide)):
            fragment1 = peptide[0:index:]  # N-terminal fragments
            fragment2 = peptide[index::]  # C-terminal fragments

            if fragment1 and not fragment1 == '_':
                result.add(tuple([fragment1, 'N']))

            if fragment2 and not fragment2 == '_':
                result.add(tuple([fragment2, 'C']))
        fragment_peptide_cache[peptide] = result
        if len(fragment_peptide_cache) > 100000:
            fragment_peptide_cache.popitem(last=False)
            # cache is to big, pop item
    return result


def max_charge(fragment):
    '''
    As a peptide usually contains the amino acids R or K (through tryptic
    digestion) and has a N-terminus (the peptide terminus, not the proteins),
    the minimum charge would be 2.
    '''
    max = 1  # starting with 1 due to the N-terminus
    for charged_aa in ['R', 'H', 'K', ]:
        max += fragment.count(charged_aa)
    return max


def unchargedFragmentMasses_with_cache(
        peptide,
        terminus_type,
        internalPrecision,
        ionsToSearch='by',
        cShift=None,
        calculateLosses=True,
        flag_18o=False):
    answer = None
    try:
        answer = fragment_ion_cache[
            (peptide, terminus_type, ionsToSearch, cShift, calculateLosses)]
    except KeyError:
        answer = list(
            unchargedFragmentMasses(
                peptide,
                terminus_type,
                internalPrecision,
                ionsToSearch,
                cShift,
                calculateLosses,
                flag_18o))
        fragment_ion_cache[
            (peptide, terminus_type, ionsToSearch, cShift, calculateLosses)] = answer

    return answer


def unchargedFragmentMasses(
        peptide,
        terminus_type,
        internalPrecision,
        ionsToSearch='by',
        cShift=None,
        calculateLosses=True,
        flag_18o=False):
    '''
    This is a generator for calculating fragment masses
    '''
    mass = peptideMassWithAdjustment_internalPrecision(
        peptide.upper(), cShift, internalPrecision)

    label = len(peptide)

    if terminus_type == 'C':
        # all c terminal ions might affected by 18O digestion
        if flag_18o:
            mass += int(round(2.00424578 * internalPrecision))
        if 'y' in ionsToSearch:
            mass_y = mass  # + ELEMENT_MASS_H
            yield mass_y, 'y{0}'.format(label)
            if calculateLosses and (
                    'S' in peptide or 'T' in peptide or 'E' in peptide or 'D' in peptide):
                yield ((mass_y
                       - ELEMENT_MASS_INTERNAL_PRECISION('H', internalPrecision) * 2
                       - ELEMENT_MASS_INTERNAL_PRECISION('O', internalPrecision)),
                      'y{0}-H2O'.format(label))
            if calculateLosses and (
                    'R' in peptide or 'K' in peptide or 'Q' in peptide or 'N' in peptide):
                yield ((mass_y
                        - ELEMENT_MASS_INTERNAL_PRECISION('N', internalPrecision)
                        - ELEMENT_MASS_INTERNAL_PRECISION('H', internalPrecision) * 3),
                       'y{0}-NH3'.format(label))
        if 'z' in ionsToSearch:
            mass_z = (mass
                      - ELEMENT_MASS_INTERNAL_PRECISION('N', internalPrecision)
                      - ELEMENT_MASS_INTERNAL_PRECISION('H', internalPrecision) * 2)
            yield mass_z, 'z{0}'.format(label)
            if calculateLosses and (
                    'S' in peptide or 'T' in peptide or 'E' in peptide or 'D' in peptide):
                yield ((mass_z
                        - ELEMENT_MASS_INTERNAL_PRECISION('H', internalPrecision) * 2
                        - ELEMENT_MASS_INTERNAL_PRECISION('O', internalPrecision)),
                       'z{0}-H2O'.format(label))
            if calculateLosses and (
                    'R' in peptide or 'K' in peptide or 'Q' in peptide or 'N' in peptide):
                yield ((mass_z
                        - ELEMENT_MASS_INTERNAL_PRECISION('N', internalPrecision)
                        - ELEMENT_MASS_INTERNAL_PRECISION('H', internalPrecision) * 3),
                       'z{0}-NH3'.format(label))
        if 'x' in ionsToSearch:
            mass_x = (mass
                      + ELEMENT_MASS_INTERNAL_PRECISION('O', internalPrecision)
                      + ELEMENT_MASS_INTERNAL_PRECISION('C', internalPrecision)
                      - ELEMENT_MASS_INTERNAL_PRECISION('H', internalPrecision) * 2)
            yield mass_x, 'x{0}'.format(label)
            if calculateLosses and (
                    'S' in peptide or 'T' in peptide or 'E' in peptide or 'D' in peptide):
                yield ((mass_x
                        - ELEMENT_MASS_INTERNAL_PRECISION('H', internalPrecision) * 2
                        - ELEMENT_MASS_INTERNAL_PRECISION('O', internalPrecision)),
                       'x{0}-H2O'.format(label))
            if calculateLosses and (
                    'R' in peptide or 'K' in peptide or 'Q' in peptide or 'N' in peptide):
                yield ((mass_x
                        - ELEMENT_MASS_INTERNAL_PRECISION('N', internalPrecision)
                        - ELEMENT_MASS_INTERNAL_PRECISION('H', internalPrecision) * 3),
                       'x{0}-NH3'.format(label))
    elif terminus_type == 'N':
        if 'b' in ionsToSearch and len(peptide) != 1:
            # only b2 and greater ions are calculated, b1 is discarded
            mass_b=mass - ELEMENT_MASS_INTERNAL_PRECISION(
                'H', internalPrecision) * 2 - ELEMENT_MASS_INTERNAL_PRECISION('O', internalPrecision)
            yield mass_b, 'b{0}'.format(label)
            if calculateLosses and (
                    'S' in peptide or 'T' in peptide or 'E' in peptide or 'D' in peptide):
                yield ((mass_b
                        - ELEMENT_MASS_INTERNAL_PRECISION('H', internalPrecision) * 2
                        - ELEMENT_MASS_INTERNAL_PRECISION('O', internalPrecision)),
                        'b{0}-H2O'.format(label))
            if calculateLosses and (
                    'R' in peptide or 'K' in peptide or 'Q' in peptide or 'N' in peptide):
                yield ((mass_b
                        - ELEMENT_MASS_INTERNAL_PRECISION('N', internalPrecision)
                        - ELEMENT_MASS_INTERNAL_PRECISION('H', internalPrecision) * 3),
                       'b{0}-NH3'.format(label))
            if calculateLosses and (
                    'R' in peptide or 'H' in peptide or 'K' in peptide):
                yield ((mass_b
                        + ELEMENT_MASS_INTERNAL_PRECISION('H', internalPrecision) * 2
                        + ELEMENT_MASS_INTERNAL_PRECISION('O', internalPrecision)),
                       'b{0}+H2O'.format(label))
        if 'a' in ionsToSearch:
            mass_a = (mass
                      - ELEMENT_MASS_INTERNAL_PRECISION('C', internalPrecision)
                      - ELEMENT_MASS_INTERNAL_PRECISION('O', internalPrecision)
                      - ELEMENT_MASS_INTERNAL_PRECISION('H', internalPrecision) * 2
                      - ELEMENT_MASS_INTERNAL_PRECISION('O', internalPrecision))
            yield mass_a, 'a{0}'.format(label)
            if calculateLosses and (
                    'S' in peptide or 'T' in peptide or 'E' in peptide or 'D' in peptide):
                yield ((mass_a
                        - ELEMENT_MASS_INTERNAL_PRECISION('H', internalPrecision) * 2
                        - ELEMENT_MASS_INTERNAL_PRECISION('O', internalPrecision)),
                        'a{0}-H2O'.format(label))
            if calculateLosses and (
                    'R' in peptide or 'K' in peptide or 'Q' in peptide or 'N' in peptide):
                yield ((mass_a
                        - ELEMENT_MASS_INTERNAL_PRECISION('N', internalPrecision)
                        - ELEMENT_MASS_INTERNAL_PRECISION('H', internalPrecision) * 3),
                       'a{0}-NH3'.format(label))
        if 'c' in ionsToSearch:
            mass_c = (mass
                      + ELEMENT_MASS_INTERNAL_PRECISION('N', internalPrecision)
                      + ELEMENT_MASS_INTERNAL_PRECISION('H', internalPrecision)
                      - ELEMENT_MASS_INTERNAL_PRECISION('O', internalPrecision))
            yield mass_c, 'c{0}'.format(label)
            if calculateLosses and (
                    'S' in peptide or 'T' in peptide or 'E' in peptide or 'D' in peptide):
                yield ((mass_c
                        - ELEMENT_MASS_INTERNAL_PRECISION('H', internalPrecision) * 2
                        - ELEMENT_MASS_INTERNAL_PRECISION('O', internalPrecision)),
                       'c{0}-H2O'.format(label))
            if calculateLosses and (
                    'R' in peptide or 'K' in peptide or 'Q' in peptide or 'N' in peptide):
                yield ((mass_c
                        - ELEMENT_MASS_INTERNAL_PRECISION('N', internalPrecision)
                        - ELEMENT_MASS_INTERNAL_PRECISION('H', internalPrecision) * 3),
                       'c{0}-NH3'.format(label))
    elif terminus_type == 'immonium':
        yield ((mass
                - ELEMENT_MASS_INTERNAL_PRECISION('C', internalPrecision)
                - ELEMENT_MASS_INTERNAL_PRECISION('O', internalPrecision) * 2
                - ELEMENT_MASS_INTERNAL_PRECISION('H', internalPrecision) * 3),
               'immonium{0}'.format(label))
        # ammonia loss
        yield ((mass
                - ELEMENT_MASS_INTERNAL_PRECISION('C', internalPrecision)
                - ELEMENT_MASS_INTERNAL_PRECISION('O', internalPrecision) * 2
                - ELEMENT_MASS_INTERNAL_PRECISION('H', internalPrecision) * 3
                - ELEMENT_MASS_INTERNAL_PRECISION('N', internalPrecision)
                - ELEMENT_MASS_INTERNAL_PRECISION('H', internalPrecision) * 3),
               'immonium{0}-NH3'.format(label))
    else:
        print('Error: terminus type not known')
        exit(1)


def chargedFragmentMasses(
        fragment_peptide,
        terminus_type,
        precursorCharge,
        internalPrecision,
        ionsToSearch='by',
        cShift=None,
        calculateLosses=True,
        flag_18o=False):
    if flag_18o:
        print('18O not supported yet for charged fragment masses. Sorry')
        sys.exit(1)
    for fragment_mass, fragment_ion in unchargedFragmentMasses(
            fragment_peptide,
            terminus_type,
            internalPrecision,
            ionsToSearch,
            cShift,
            calculateLosses):
        max_fragment_charge=max_charge(fragment_peptide)
        if max_fragment_charge > precursorCharge:
            max_fragment_charge=precursorCharge
        for z in range(1, max_fragment_charge + 1):
            charged_fragment_mass=((fragment_mass
                                    + z * proton_mass_internal_precision[internalPrecision])
                                   / z)
            yield charged_fragment_mass, '{0} (+{1})'.format(fragment_ion, z)


def getPeptidesFromFasta(
        fastaFile,
        missedCleavageSites,
        enzyme,
        notCleavedAminoAcids,
        minPeptideLength):
    '''
    Parse a fasta file and return all peptides
    Requires a path to a single fasta files
    Returns a set of all peptides
    Peptides are uniq, digested with specified enzyme, missed cleavages, 
    not cleaved after amino acids and minimum peptide length as specified
    '''
    print('Reading protein database.')
    allpeptides=fasta.parseFastaAndDigest(
        fastaFile,
        missedCleavageSites,
        enzyme,
        notCleavedAminoAcids,
        NTerminus=True)
    print('Identified ' + str(len(allpeptides)) +
          ' peptides (with digestion and missed cleavages).')
    peptides2delete=list()
    for peptide in allpeptides:
        if peptide and peptide[0] == '_':
            # peptide contains N terminus
            if len(peptide) <= minPeptideLength:
                peptides2delete.append(peptide)
        else:
            # peptide contains no N terminus
            if len(peptide) < minPeptideLength:
                peptides2delete.append(peptide)
    for peptide in peptides2delete:
        allpeptides.discard(peptide)
    print('Identified ' + str(len(allpeptides)) +
          ' peptides (after deleting short peptides).')
    return allpeptides


def deletePeptidesAndCalculateMass(
        allpeptides,
        crosslinkedAA,
        cShift,
        internalPrecision=None,
        flag_18o=False):
    '''Delete all peptides which don't contain the crosslinked amino acids
    Calculate the mass of the remaining peptides
    Returns a list containing:
                                [0] = set of allpeptides
                                [1] = mass2peptides (dict: key = mass, 
                                                           value = list of peptides)
                                [2] = peptide2mass (dict: key = peptide,
                                                          value = mass)
    '''
    mass2peptides=defaultdict(list)
    peptide2mass=dict()

    # delete all peptides which do not include the crosslinked amino acids
    # and calculate masses for each peptide
    peptides2delete=list()
    for peptide in allpeptides:
        # skip all peptides which do not include the crosslinked amino acids
        skip=True
        for aa in crosslinkedAA:
            if aa in peptide[:-1]:  
                # only consider peptide with minimum one missed cleavage
                # (as only those can have crosslinks)
                skip=False
                break

        # also skip peptides containing X as an amino acid
        if skip == False and 'X' in peptide:
            skip=True

        if skip:
            peptides2delete.append(peptide)
            continue

        if internalPrecision is not None:
            mass=peptideMassWithAdjustment_internalPrecision(
                peptide, cShift, internalPrecision, flag_18o=flag_18o)
        else:
            # mass = peptideMassWithAdjustment(peptide, cShift)
            raise NotImplementedError
        mass2peptides[mass].append(peptide)
        peptide2mass[peptide]=mass

    # delete peptides which do not contain crosslinked amino acids
    for peptide in peptides2delete:
        allpeptides.discard(peptide)

    return [tuple(allpeptides), mass2peptides, peptide2mass]


def peptideMassWithAdjustment_internalPrecision(
        peptide, cShift, internalPrecision, flag_18o=False):
    # adjust the peptide sequence (delete '_') for calculating the mass
    if peptide[0] == '_':
        mass=proteomicsKnowledge.peptideMass_internalPrecision(
            peptide[1::], internalPrecision, flag_18O=flag_18o)
    else:
        mass=proteomicsKnowledge.peptideMass_internalPrecision(
            peptide, internalPrecision, flag_18O=flag_18o)

    if cShift:
        mass += peptide.count('C') * \
            int(round(CARBOXAMIDOMETHYL_SHIFT * internalPrecision))
    return mass


def lowercaseCrosslinkPosition(peptide, position):
    # returns a peptide string, were the cross-linked amino acid is in lower
    # case
    peptide_list=list(peptide)
    peptide_list[position]=peptide_list[position].lower()
    return ''.join(peptide_list)

def ion_series(ion_string_list, compiled_re_ion_series):
    '''
    Search for ion series in a list of ions.
    Returns a set of ion series.
    '''

    ion_series = list()
    for ion_string in ion_string_list:
        for re_object in compiled_re_ion_series.finditer(ion_string):
            # re searches for y1, y12, a1, ...
            # re returns 1, 12, 1

            ion_series.append([])
            ion_series[-1].append(ion_string)

            start = re_object.start()
            end = re_object.end()
            match = int(re_object.group(0))

            ion_string_as_list = list(ion_string)
            for _i in range(start + 1, end):
                ion_string_as_list.pop(start)

            tmp_match = match
            while match > 1:
                tmp_match = tmp_match - 1
                ion_string_as_list[start] = str(tmp_match)
                new_ion_string = ''.join(ion_string_as_list)
                if new_ion_string in ion_string_list:
                    ion_series[-1].append(new_ion_string)
                else:
                    break

            tmp_match = match
            while True:
                tmp_match += 1
                ion_string_as_list[start] = str(tmp_match)
                new_ion_string = ''.join(ion_string_as_list)
                if new_ion_string in ion_string_list:
                    ion_series[-1].append(new_ion_string)
                else:
                    break

    answer = set()
    for _ in ion_series:
        if len(_) > 1:
            answer.add(tuple(sorted(_)))
    return answer
    
def internal_precision(mass, internal_precision):
    '''
    Calculate the mass with internal precision.
    Returns an integer.
    '''
    return int(round(mass * internal_precision))

def search_precursor(
        crosslinker_list_internal_precision_ms1,
        transformed_precursor_mass_error,
        max_peptide_mass,
        min_peptide_mass,
        len_peptide_mass_list,
        peptide_mass_list_masses,
        transformed_precursor_mass,
        isotope_list_internal_precision_ms1):
    for transformed_crosslinker_mass, crosslinker_form_ in crosslinker_list_internal_precision_ms1:
        for transformed_isotope_difference, isotope_number in isotope_list_internal_precision_ms1:
            matching_peptide_combinations = set()
            crosslinker_form = (crosslinker_form_, isotope_number)
            transformed_precursor_mass_no_crosslinker = (transformed_precursor_mass 
                                                         - transformed_crosslinker_mass 
                                                         + transformed_isotope_difference)

            index_min = 0
            index_max = len_peptide_mass_list - 1

            # first check: can the precursor peak be explained with
            # crosslinking of the minumum or the maximum peptide mass
            if transformed_precursor_mass_no_crosslinker + \
                    transformed_precursor_mass_error > max_peptide_mass * 2:
                continue
            elif transformed_precursor_mass_no_crosslinker - transformed_precursor_mass_error < min_peptide_mass * 2:
                continue

            # binary search for the middle of the range
            target = int(
                round(
                    (transformed_precursor_mass_no_crosslinker)
                    / 2))
            start = bisect_left(peptide_mass_list_masses, target)

            diff2target = (transformed_precursor_mass_no_crosslinker
                           - peptide_mass_list_masses[start]
                           - peptide_mass_list_masses[start])

            if diff2target < transformed_precursor_mass_error:
                while 0 > diff2target < transformed_precursor_mass_error:
                    start = start - 1
                    last_diff2target = diff2target
                    diff2target = (transformed_precursor_mass_no_crosslinker
                                   - peptide_mass_list_masses[start]
                                   - peptide_mass_list_masses[start])
                try:
                    if abs(last_diff2target) < abs(diff2target):
                        start += 1
                except UnboundLocalError:
                    # rescue diff2target < 0, should be faster than if
                    pass

            elif diff2target > transformed_precursor_mass_error:
                #print('increasing start')
                while 0 < diff2target > transformed_precursor_mass_error:
                    start += 1
                    last_diff2target = diff2target
                    diff2target = (transformed_precursor_mass_no_crosslinker
                                   - peptide_mass_list_masses[start]
                                   - peptide_mass_list_masses[start])
                try:
                    if abs(last_diff2target) < abs(diff2target):
                        start = start - 1
                except UnboundLocalError:
                    # rescue diff2target < 0, should be faster than if
                    pass

            index_min = start
            index_max = start

            len_peptide_mass_list_masses = len(peptide_mass_list_masses)

            max_for_combination = (transformed_precursor_mass_no_crosslinker
                                   + transformed_precursor_mass_error 
                                   - min_peptide_mass)
            while index_max < len_peptide_mass_list_masses and peptide_mass_list_masses[
                    index_max] <= max_for_combination and index_min >= 0:
                diff2target = (transformed_precursor_mass_no_crosslinker
                               - peptide_mass_list_masses[index_max]
                               - peptide_mass_list_masses[index_min])
                if abs(diff2target) <= transformed_precursor_mass_error:
                    # MATCH
                    matching_peptide_combinations.add(
                        (index_max, index_min))

                    # index_min -1
                    try:
                        temp_index_min = index_min
                        while True:
                            temp_index_min = temp_index_min - 1
                            diff2target = (
                                transformed_precursor_mass_no_crosslinker
                                - peptide_mass_list_masses[index_max]
                                - peptide_mass_list_masses[temp_index_min])
                            if abs(
                                    diff2target) <= transformed_precursor_mass_error:
                                matching_peptide_combinations.add(
                                    (index_max, temp_index_min))
                                temp_index_max = index_max
                                while True:
                                    temp_index_max = temp_index_max - 1
                                    diff2target = (
                                        transformed_precursor_mass_no_crosslinker
                                        - peptide_mass_list_masses[temp_index_max]
                                        - peptide_mass_list_masses[temp_index_min])
                                    if abs(
                                            diff2target) <= transformed_precursor_mass_error:
                                        matching_peptide_combinations.add(
                                            (temp_index_max, temp_index_min))
                                    else:
                                        break
                            else:
                                break
                    except IndexError:
                        pass

                    # index_max + 1
                    try:
                        temp_index_max = index_max
                        while True:
                            temp_index_max += 1
                            diff2target = (
                                transformed_precursor_mass_no_crosslinker
                                - peptide_mass_list_masses[temp_index_max]
                                - peptide_mass_list_masses[index_min])
                            if abs(
                                    diff2target) <= transformed_precursor_mass_error:
                                matching_peptide_combinations.add(
                                    (temp_index_max, index_min))
                                temp_index_min = index_min
                                while True:
                                    temp_index_min += 1
                                    diff2target = (
                                        transformed_precursor_mass_no_crosslinker
                                        - peptide_mass_list_masses[temp_index_max]
                                        - peptide_mass_list_masses[temp_index_min])
                                    if abs(
                                            diff2target) <= transformed_precursor_mass_error:
                                        matching_peptide_combinations.add(
                                            (temp_index_max, temp_index_min))
                                    else:
                                        break
                            else:
                                break
                    except IndexError:
                        pass

                    # index_max -1 / index_min - 1
                    try:
                        temp_index_min = index_min
                        temp_index_max = index_max
                        while True:
                            temp_index_min = temp_index_min - 1
                            temp_index_max = temp_index_max - 1
                            diff2target = (
                                transformed_precursor_mass_no_crosslinker
                                - peptide_mass_list_masses[temp_index_max]
                                - peptide_mass_list_masses[temp_index_min])
                            if abs(
                                    diff2target) <= transformed_precursor_mass_error:
                                matching_peptide_combinations.add(
                                    (temp_index_max, temp_index_min))
                            else:
                                break

                        # index_max +1 / index_min + 1
                        temp_index_min = index_min
                        temp_index_max = index_max
                        while True:
                            temp_index_min = temp_index_min + 1
                            temp_index_max = temp_index_max + 1
                            diff2target = (
                                transformed_precursor_mass_no_crosslinker 
                                - peptide_mass_list_masses[temp_index_max]
                                - peptide_mass_list_masses[temp_index_min])
                            if abs(
                                    diff2target) <= transformed_precursor_mass_error:
                                matching_peptide_combinations.add(
                                    (temp_index_max, temp_index_min))
                            else:
                                break
                    except IndexError:
                        pass

                    index_min = index_min - 1
                    index_max += 1

                elif diff2target < 0:
                    index_min = index_min - 1
                elif diff2target > 0:
                    index_max += 1

            yield crosslinker_form, matching_peptide_combinations