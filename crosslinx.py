'''
Copyright (c) 2011-2019 Till Bald

Crosslinx is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

Crosslinx is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Crosslinx.  If not, see <http://www.gnu.org/licenses/>.
'''

from __future__ import print_function
import sys
import os
import csv
import heapq  # for  heapq.nlargest()
import re
from copy import deepcopy
from operator import itemgetter as itemgetter
from collections import defaultdict

# change the current directory so that all submodules etc can work as usual
# the current directory has to be the directory where crosslinx.py is stored
if getattr(sys, 'frozen', False):
    # we have a crosslinx executable created by pyinstaller
    CURRENT_FILE_PATH = sys._MEIPASS
else:
    # normal python script
    CURRENT_FILE_PATH = os.path.dirname(os.path.realpath(__file__))
os.chdir(CURRENT_FILE_PATH)
del CURRENT_FILE_PATH

# add include/python add the beginning of sys. path
# to make sure that the modules distributed with crosslinx are used
# rather than those found on the users system.
sys.path = ['./include/python'] + sys.path
# pylint: disable=wrong-import-position
import proteomatic
import fasta
import spectra
import crosslinking
import proteomicsKnowledge
import pymzml
# pylint: enable=wrong-import-position

# Ensure the user is running the version of python we require.
if not hasattr(sys, "version_info") or sys.version_info < (3, 0):
    raise RuntimeError("Crosslinx requires Python 3.0 or later.")

PROTON_MASS = proteomicsKnowledge.PROTON_MASS
PROTON_MASS_INTERNAL_PRECISION = proteomicsKnowledge.proteomicsKnowledge[
    'protonMass_internal']
ISOTOPE_AVERAGE_DIFFERENCE = 1.002

class Crosslinx(proteomatic.ProteomaticScript):
    '''
    Class to instantiate Proteomatic background
    '''

    def run(self):
        '''
        Run the actual script for Crosslinx.
        '''
        # setting up some variables for speed reasons
        param_precision_ms1 = self.param['ppmMS1'] * 1e-6
        param_precision_ms2 = self.param['ppmMS2'] * 1e-6
        param_c_shift = self.param['cShift']
        param_minimum_matches = self.param['minimumMatches']
        param_decoy_on_demand = self.param['decoy_on_demand']
        param_18o = self.param['18O']

        self.uncharged_fragment_masses = crosslinking.unchargedFragmentMasses
        if self.param['cache']:
            # enabling caching
            self.uncharged_fragment_masses = crosslinking.unchargedFragmentMasses_with_cache

        # get internal precisions from MS1 and MS2 spectra
        precision_ms1_internal, precision_ms2_internal = self.internal_precision()
        self.precision_ms1_internal = precision_ms1_internal
        self.precision_ms2_internal = precision_ms2_internal

        self.crosslinked_aa = self.param['crosslinkedAA'].split(' ')[0]

        # compile regular expression for later ...
        compiled_re_crosslinked_aa_in_sequence = re.compile(
            '[' + self.crosslinked_aa + r']\w')
        compiled_re_crosslinked_aa = re.compile(
            '[' + self.crosslinked_aa.lower() + ']')
        compiled_re_ion_series = re.compile(r'(?<=[abcxyz])+\d+')

        # get cross-linker masses
        crosslinkermass = float(self.param['crossLinkerMass'].split(' ')[0])
        crosslinkermassshift = float(
            self.param['crossLinkerMassShift'].split(' ')[0])
        if crosslinkermassshift != 0:
            crosslinker_list_internal_precision_ms1 = (
                (int(
                    round(
                        crosslinkermass * precision_ms1_internal)),
                 'light'),
                (int(
                    round(
                        (crosslinkermass + crosslinkermassshift) * precision_ms1_internal)),
                 'heavy'))
        else:
            crosslinker_list_internal_precision_ms1 = (
                (int(round(crosslinkermass * precision_ms1_internal)), 'light'),)

        # diagnostic ions
        diagnostic_ions = self.diagnostic_ions(crosslinkermassshift)

        # isoptope list
        # create isotope_list_internal_precision_ms1
        # [(isotope mass, number of isotopes), ...]
        isotope_average_difference_ip_ms1 = int(
            round(ISOTOPE_AVERAGE_DIFFERENCE * precision_ms1_internal))
        isotope_list_internal_precision_ms1 = [(0, 0)]
        i = 1
        while i <= self.param['maxIsotopeNumber']:
            isotope_list_internal_precision_ms1.append(
                (isotope_list_internal_precision_ms1[-1][0] + isotope_average_difference_ip_ms1, i))
            i += 1

        # set plot_flag (plotting of every MS2 spectrum with a match, and
        # corresponding MS1 spectrum)
        plot_flag = self.param['plot']

        allpeptides1, fasta_info1, peptide2mass1 = self.get_all_peptides(self.input['fastaFilesA'])
        allpeptides2, fasta_info2, peptide2mass2 = self.get_all_peptides(self.input['fastaFilesB'])

        # allpeptide now contains a set of all peptides (digested, with missed
        # cleavages, not cleaved after amino acid, minimum peptide length)

        # just check if the fasta filenames are the same,
        different_fasta_files = False
        if allpeptides1 != allpeptides2:
            different_fasta_files = True

        # this crosslinks every peptide in fastaFilesA to fastaFilesA and B,
        # the same for B to A and B
        peptide_mass_list = list()
        for peptide, mass in peptide2mass1.items():
            peptide_mass_list.append((mass, peptide))

        if different_fasta_files:
            for peptide, mass in peptide2mass2.items():
                if peptide not in peptide2mass1:
                    peptide_mass_list.append((mass, peptide))

        del peptide2mass1
        del peptide2mass2

        if different_fasta_files:
            # keep the peptide information from different files, to simply
            # check for it later
            pass
        else:
            # no check needed from which file the peptides stem from, delete it
            del allpeptides1
            del allpeptides2

        # delete peptides, which are too big
        peptide_mass_list = self.delete_big_peptides(peptide_mass_list)

        len_peptide_mass_list = len(peptide_mass_list)
        max_peptide_mass = peptide_mass_list[-1][0]
        min_peptide_mass = peptide_mass_list[0][0]

        peptide_mass_list_masses, peptide_mass_list_peptides = zip(
            *peptide_mass_list)

        print('Identified {} peptides to crosslink with each other'.format(
            len_peptide_mass_list))
        sys.stdout.flush()

        print('Generating cross-link sites on all peptides ...', end='\r')
        sys.stdout.flush()
        # Generate crosslink sites by lowercasing the cross-linked amino acid
        peptide_mass_list_peptides_crosslink_sites = []
        for peptide in peptide_mass_list_peptides:
            peptide_mass_list_peptides_crosslink_sites.append([])
            crosslink_position_generator = (
                m.start() for m in compiled_re_crosslinked_aa_in_sequence.finditer(peptide))
            for i in crosslink_position_generator:
                peptide_mass_list_peptides_crosslink_sites[-1].append(
                    crosslinking.lowercaseCrosslinkPosition(peptide, i))
                # string where the crosslinked amino acid is in lowercase
        print('Generating cross-link sites on all peptides done!')
        sys.stdout.flush()

        # write header for output file
        if 'identified_crosslinks_perScan' in self.output.keys():
            with open(self.output['identified_crosslinks_perScan'], 'w', newline='') as f:
                output_csv_per_scan = csv.writer(f)
                output_csv_per_scan.writerow(
                    [
                        'insilico_cl_peptide1',
                        'insilico_cl_peptide2',
                        'scan',
                        'sum intensities',
                        'count matches',
                        'E value',
                        'defline',
                        'protein1',
                        'protein2',
                        'file',
                        'Theo mass',
                        'Mass',
                        'measured precursor mz',
                        'charge',
                        'delta ppm',
                        'other MS1 peak',
                        'crosslinkerForm',
                        'filename/id',
                        'Mods',
                        'peptide',
                        'ion series',
                        'n ion series',
                        'MS1 isotope',
                        'Diagnostic ions'])

        omssa_dict, _ = self.input_reader(self.input['omssaFiles'])
        crosslink_dict, crosslink_dict_peptide_combination = self.input_reader(self.input['crosslinkFiles'])

        accept_scans = False
        # read crosslink files:
        if len(self.input['crosslinkFiles']) != 0:
            accept_scans = True

        # read mzMLFiles and store data
        for position, mzml_file in enumerate(self.input['mzMLFiles']):
            print('')
            skipped_n = 0
            skipped_n_cl = 0
            mzml_file_short = mzml_file.split('/')[-1].split('.')[0]
            print('({}/{}) {}'.format(position + 1,
                                      len(self.input['mzMLFiles']),
                                      mzml_file_short))
            sys.stdout.flush()
            last_ms1_spectrum = None
            count_scans = 0

            # read all spectra
            for s in pymzml.run.Reader(
                    mzml_file,
                    MS1_Precision=param_precision_ms1,
                    MSn_Precision=param_precision_ms2):
                if s['ms level'] == 1:
                    last_ms1_spectrum = deepcopy(s)
                    # s.deRef() decodes the binary data - this is not necessary
                    # here
                elif s['ms level'] == 2:
                    if s['id'] in omssa_dict[mzml_file_short]:
                        # skip this scan
                        skipped_n += 1
                        continue
                    if accept_scans and s[
                            'id'] not in crosslink_dict[mzml_file_short]:
                        skipped_n_cl += 1
                        continue

                    if len(s['precursors']) == 1:
                        precursor_mz = s['precursors'][0]['mz']
                        charge = s['precursors'][0]['charge']
                        try:
                            precursor_mass_measured = (
                                precursor_mz - PROTON_MASS) * charge
                        except TypeError:
                            print('ERROR!')
                            print(
                                'The scan #{0} does not have a valid charge. The charge value is "{1}".'.format(
                                    s['id'], charge))

                        count_scans += 1
                        print(
                            'Calculated ' +
                            str(count_scans) +
                            ' scans.',
                            end='\r')
                        sys.stdout.flush()

                        spectrum_mass_tuple = []

                        # filter noise
                        if self.param['removeNoise']:
                            s.removeNoise('median')

                        deconvolution_flag = self.param['deconvolution']
                        if deconvolution_flag:
                            # deconvolution
                            spectrum_mass_tuple = s.deconvolutedPeaks
                        else:
                            spectrum_mass_tuple = s.centroidedPeaks

                        # check minimum number of peaks
                        n_peaks = len(spectrum_mass_tuple)
                        if n_peaks < param_minimum_matches:
                            continue
                            # skip this

                        transformed_precursor_mass = int(
                            round(precursor_mass_measured * precision_ms1_internal))
                        transformed_precursor_mass_error = int(
                            round(
                                precursor_mass_measured *
                                param_precision_ms1 *
                                precision_ms1_internal))

                        temp_results_for_this_scan = []
                        max_mass_or_mz = max(spectrum_mass_tuple)[0]

                        intensity_sum_filtered_spec = 0
                        for (mass, intensity) in spectrum_mass_tuple:
                            intensity_sum_filtered_spec += intensity

                        # identify the three highest peaks
                        highest_peaks_transformed_with_error = self.highest_peaks(spectrum_mass_tuple, 3)

                        # initialize plot objects as none if plotting is not required
                        # these plot objects are handed to match_to_spectrum()
                        # handing over none objects should be faster than
                        # another check for plot_flag
                        plot_object = None
                        if plot_flag:
                            plot_object = pymzml.plot.Factory()

                        scan_id = s['id']
                        crosslinker_form_counter = 0

                        for (crosslinker_form, isotope_number), matching_peptide_combinations in crosslinking.search_precursor(
                                crosslinker_list_internal_precision_ms1,
                                transformed_precursor_mass_error,
                                max_peptide_mass,
                                min_peptide_mass,
                                len_peptide_mass_list,
                                peptide_mass_list_masses,
                                transformed_precursor_mass,
                                isotope_list_internal_precision_ms1):
                            print('')
                            print('Analyzing cross-linker form/isotope number {}/6 ({}/{})'.format(
                                crosslinker_form_counter + 1, crosslinker_form, isotope_number))
                            sys.stdout.flush()
                            crosslinker_form_counter += 1
                            theoretical_precursor_counter = 0
                            percantage_of_precursor_previous = 0.0
                            temp_len_matching_peptide_combinations = len(
                                matching_peptide_combinations)

                            tmp_crosslinker_mass = int(
                                round(crosslinkermass * precision_ms2_internal))
                            if crosslinker_form == 'heavy':
                                tmp_crosslinker_mass += int(
                                    round(crosslinkermassshift * precision_ms2_internal))

                            for insilico_cl_peptide1_index, insilico_cl_peptide2_index in matching_peptide_combinations:
                                if different_fasta_files:
                                    peptide1 = peptide_mass_list_peptides[
                                        insilico_cl_peptide1_index]
                                    peptide2 = peptide_mass_list_peptides[
                                        insilico_cl_peptide2_index]

                                    # peptide1 has to be in fasta1, and
                                    # peptide2 in fasta2 or vice versa
                                    if peptide1 in allpeptides1 and peptide2 in allpeptides2:
                                        pass
                                    elif peptide1 in allpeptides2 and peptide2 in allpeptides1:
                                        pass
                                    else:
                                        # skip this peptide combination
                                        continue

                                percantage__of_precursor = (
                                    theoretical_precursor_counter
                                    / temp_len_matching_peptide_combinations
                                    * 100)
                                if temp_len_matching_peptide_combinations > 500 and percantage__of_precursor > percantage_of_precursor_previous:
                                    print(
                                        'Analyzing precursor {}/{} ({:6.2f}%)'.format(
                                            theoretical_precursor_counter,
                                            temp_len_matching_peptide_combinations,
                                            percantage__of_precursor),
                                        end='\r')
                                    sys.stdout.flush()
                                    percantage_of_precursor_previous += 0.01
                                theoretical_precursor_counter += 1
                                # peptide_mass_list_peptides_crosslink_sites
                                # contains lists of peptidestrings where the
                                # crosslinked amino acid is in lowercase
                                for peptide1 in peptide_mass_list_peptides_crosslink_sites[
                                        insilico_cl_peptide1_index]:
                                    for peptide2 in peptide_mass_list_peptides_crosslink_sites[
                                            insilico_cl_peptide2_index]:
                                        if accept_scans and plot_flag:
                                            # need to analyze only those peptide
                                            # combinations that are in
                                            # crosslink_dict_peptide_combination
                                            # check both combinations of
                                            # peptide1/peptide2 just to be
                                            # sure.
                                            valid_peptide = False
                                            if (peptide1.upper(), peptide2.upper()) in crosslink_dict_peptide_combination[
                                                    mzml_file_short][s['id']]:
                                                valid_peptide = True
                                            elif (peptide2.upper(), peptide1.upper()) in crosslink_dict_peptide_combination[mzml_file_short][s['id']]:
                                                valid_peptide = True
                                            if not valid_peptide:
                                                continue

                                        target_or_decoy = dict()
                                        target_or_decoy['target'] = (
                                            peptide1, peptide2)

                                        # on demand decoy
                                        if param_decoy_on_demand:
                                            target_or_decoy['decoy'] = (
                                                fasta.reversePeptide(peptide1), fasta.reversePeptide(peptide2))

                                        for td_string, (peptide1, peptide2) in target_or_decoy.items():
                                            # start multithreading here?
                                            result = self.match_to_spectrum(
                                                peptide1,
                                                peptide2,
                                                td_string,
                                                tmp_crosslinker_mass,
                                                precision_ms2_internal,
                                                compiled_re_crosslinked_aa,
                                                diagnostic_ions[crosslinker_form],
                                                param_18o,
                                                s,
                                                highest_peaks_transformed_with_error,
                                                charge,
                                                deconvolution_flag,
                                                plot_flag,
                                                param_minimum_matches,
                                                param_c_shift,
                                                precision_ms1_internal,
                                                intensity_sum_filtered_spec,
                                                crosslinker_form,
                                                compiled_re_ion_series,
                                                isotope_number,
                                                last_ms1_spectrum,
                                                crosslinkermassshift,
                                                plot_object)
                                            if result:
                                                temp_results_for_this_scan.append(
                                                    result)

                        if plot_flag and len(plot_object.plots) > 0:
                            plot_object.save(
                                filename='{0}_{1}_{2}.xhtml'.format(
                                    self.output['identified_crosslinks_perScan'].split('.csv')[0],
                                    mzml_file_short,
                                    scan_id))

                        if len(temp_results_for_this_scan) != 0:
                            # there are results for this scan
                            # write output (score is calculated there)
                            self.write_output(
                                results_one_scan=temp_results_for_this_scan,
                                scan_id=scan_id,
                                measured_precursor_mass=precursor_mass_measured,
                                charge=charge,
                                measured_precursor_mz=precursor_mz,
                                fasta_info1=fasta_info1,
                                fasta_info2=fasta_info2,
                                mzml_file=mzml_file,
                                max_mass_or_mz=max_mass_or_mz,
                                n_peaks=n_peaks)
                            del temp_results_for_this_scan

            print('')
            print('Skipped {} scans, because of OMSSA results'.format(skipped_n))
            if len(crosslink_dict[mzml_file_short]) != 0:
                print(
                    'Skipped {} scans, because of crosslink results, {} scans remaining'.format(
                        skipped_n_cl, len(crosslink_dict)))

    def match_to_spectrum(
            self,
            peptide1,
            peptide2,
            td_string,
            tmp_crosslinker_mass,
            precision_ms2_internal,
            compiled_re_crosslinked_aa,
            diagnostic_ions_crosslinker_form,
            param_18o,
            s,
            highest_peaks_transformed_with_error,
            charge,
            deconvolution_flag,
            plot_flag,
            param_minimum_matches,
            param_c_shift,
            precision_ms1_internal,
            intensity_sum_filtered_spec,
            crosslinker_form,
            compiled_re_ion_series,
            isotope_number,
            last_ms1_spectrum,
            crosslinkermassshift,
            plot_object):
        '''
        Match peptide combination to spectrum
        '''
        intensity_sum = 0
        matched_measured_peaks = []
        observed_mass_to_ion = defaultdict(list)
        observed_ions = list()

        t_fragment_mass_to_ion = None
        t_fragment_masses = None
        max_charge = 0
        set_intersection = None
        if deconvolution_flag:
            t_fragment_mass_to_ion = self.calc_all_fragments(
                peptide1,
                peptide2,
                tmp_crosslinker_mass,
                max_charge,
                precision_ms2_internal,
                compiled_re_crosslinked_aa,
                diagnostic_ions=diagnostic_ions_crosslinker_form,
                flag_18o=param_18o)
            t_fragment_masses = set(t_fragment_mass_to_ion.keys())

            if len(t_fragment_masses &
                   highest_peaks_transformed_with_error) == 0:
                return None

            set_intersection = t_fragment_masses & s.tmassSet
            if len(set_intersection) < param_minimum_matches:
                return None
        else:
            max_charge = charge
            t_fragment_mass_to_ion = self.calc_all_fragments(
                peptide1,
                peptide2,
                tmp_crosslinker_mass,
                max_charge,
                precision_ms2_internal,
                compiled_re_crosslinked_aa,
                diagnostic_ions=diagnostic_ions_crosslinker_form,
                flag_18o=param_18o)
            t_fragment_masses = set(t_fragment_mass_to_ion.keys())

            if len(t_fragment_masses &
                   highest_peaks_transformed_with_error) == 0:
                return None

            set_intersection = t_fragment_masses & s.tmzSet
            if len(set_intersection) < param_minimum_matches:
                return None

        for t_insilico_mass in set_intersection:
            # pylint: disable=protected-access
            mass_intensity_tuple = None
            if deconvolution_flag:
                # hasDeconvolutedPeaks
                mass_intensity_tuple = s._transformed_mass_with_error[
                    t_insilico_mass]
            else:
                # hasPeak
                mass_intensity_tuple = s._transformed_mz_with_error[
                    t_insilico_mass]
            if len(mass_intensity_tuple) != 1:
                continue

            (observed_mass_no_error, intensity) = mass_intensity_tuple[0]
            if observed_mass_no_error not in matched_measured_peaks:
                matched_measured_peaks.append(observed_mass_no_error)
                intensity_sum += intensity
            # else would be:
                # already analysed this peak, just store the fragment ion
                # information for plotting

            observed_ions += t_fragment_mass_to_ion[t_insilico_mass]

            if plot_flag:
                observed_mass_to_ion[observed_mass_no_error].append(
                    t_fragment_mass_to_ion[t_insilico_mass])

        n_successfull_product_ion_matches = len(matched_measured_peaks)
        if n_successfull_product_ion_matches >= param_minimum_matches:
            insilico_precursor_mass = (((
                crosslinking.peptideMassWithAdjustment_internalPrecision(
                    peptide1.upper(),
                    param_c_shift,
                    precision_ms1_internal)
                + crosslinking.peptideMassWithAdjustment_internalPrecision(
                    peptide2.upper(),
                    param_c_shift,
                    precision_ms1_internal)
                ) / float(precision_ms1_internal)) + (tmp_crosslinker_mass
                                                      / float(precision_ms2_internal)))

            # search for precursor mass shift
            insilico_precursor_mz = (
                insilico_precursor_mass / charge) + PROTON_MASS
            try:
                if crosslinker_form == 'light':
                    other_ms1_peak = last_ms1_spectrum.hasPeak(
                        insilico_precursor_mz + crosslinkermassshift / charge)
                else:
                    other_ms1_peak = last_ms1_spectrum.hasPeak(
                        insilico_precursor_mz - crosslinkermassshift / charge)
            except AttributeError:
                other_ms1_peak = 'no MS1 scan found'

            # get diagnostic ions
            # match diagnastic ions to the former intersection of all fragment
            # ions (including diagnostic ions) to spectrum
            # set_intersection contains transformed peaks
            matched_diagnostic_ions = diagnostic_ions_crosslinker_form.keys() & set_intersection
            diagnostic_ion_list = []
            for matched_diagnostic_ion in matched_diagnostic_ions:
                diagnostic_ion_list.append(
                    diagnostic_ions_crosslinker_form[matched_diagnostic_ion])
            diagnostic_ion_string = ', '.join(diagnostic_ion_list)

            if plot_flag:
                try:
                    fragmented_ms1_peak = last_ms1_spectrum.hasPeak(
                        insilico_precursor_mz)

                    if crosslinker_form == 'light':
                        other_ms1_peak = last_ms1_spectrum.hasPeak(
                            insilico_precursor_mz + crosslinkermassshift / charge)
                    else:
                        other_ms1_peak = last_ms1_spectrum.hasPeak(
                            insilico_precursor_mz - crosslinkermassshift / charge)

                except AttributeError:
                    fragmented_ms1_peak = False
                    other_ms1_peak = False

                if fragmented_ms1_peak:
                    mz_range = [
                        fragmented_ms1_peak[0][0] - 10,
                        fragmented_ms1_peak[0][0] + 10]
                    plot_object.newPlot(
                        header='MS1: charge {0}, {1}@{2}'.format(
                            charge,
                            peptide1,
                            peptide2,),
                        mzRange=mz_range)
                    plot_object.add(
                        last_ms1_spectrum.centroidedPeaks, color=(
                            200, 0, 0), style='sticks')
                    plot_object.add(
                        [(fragmented_ms1_peak[0][0], 'fragmented peak')],
                        color=(78, 154, 6),
                        style='label')
                    if other_ms1_peak:
                        plot_object.add(
                            [(other_ms1_peak[0][0], 'other peak')],
                            color=(164, 0, 0), style='label')

                # spectrum_mass_tuple holds all peaks, either centroided or
                # deconvoluted
                spectrum_mass_tuple = []
                if deconvolution_flag:
                    spectrum_mass_tuple = s.deconvolutedPeaks
                else:
                    spectrum_mass_tuple = s.centroidedPeaks

                plot_object.newPlot(
                    header='MS2: {0}@{1}, number of matches: {2}'.format(
                        peptide1,
                        peptide2,
                        n_successfull_product_ion_matches))
                plot_object.add(
                    spectrum_mass_tuple, color=(
                        200, 0, 0), style='sticks')

                label_list = []
                for (mz, fragment_ion_list) in observed_mass_to_ion.items():
                    fragment_ion_list = sorted(fragment_ion_list)
                    new_fragment_ion_list = []
                    for sublist in fragment_ion_list:
                        new_fragment_ion_list.append(sorted(sublist))

                    try:
                        label_list.append(
                            (mz, ', '.join([item for sublist in new_fragment_ion_list for item in sublist])))
                    except TypeError:
                        print(mz)
                        print(new_fragment_ion_list)
                        exit(1)

                plot_object.add(label_list, color=(0, 150, 0), style='label')

            return [peptide1,
                    peptide2,
                    len(t_fragment_mass_to_ion),
                    intensity_sum / intensity_sum_filtered_spec,
                    n_successfull_product_ion_matches,
                    insilico_precursor_mass,
                    other_ms1_peak,
                    crosslinker_form,
                    crosslinking.ion_series(observed_ions, compiled_re_ion_series),
                    isotope_number,
                    td_string,
                    diagnostic_ion_string]

    def write_output(
            self,
            results_one_scan=None,
            scan_id=None,
            measured_precursor_mass=None,
            charge=None,
            measured_precursor_mz=None,
            fasta_info1=None,
            fasta_info2=None,
            mzml_file=None,
            max_mass_or_mz=None,
            n_peaks=None):
        '''
        Write output for a single spectrum.
        '''
        if 'identified_crosslinks_perScan' in self.output.keys():
            param_decoy_on_demand = self.param['decoy_on_demand']
            with open(self.output['identified_crosslinks_perScan'], 'a', newline='') as f:
                output_csv_per_scan = csv.writer(f)
                # the header is written at the beginning of this script ...
                # ['insilico_cl_peptide1', 'insilico_cl_peptide2', 'scan',
                # 'sum intensities', 'count matches', 'E value', 'defline',
                # 'protein1', 'protein2', 'file', 'Theo mass', 'Mass',
                # 'measured precursor mz', 'charge', 'delta ppm',
                # 'other MS1 peak', 'crosslinkerForm', 'filename/id', 'Mods',
                # 'peptide', 'ion series', 'n ion series']

                n_theoretical_spectra = len(results_one_scan)

                for insilico_cl_peptide1, insilico_cl_peptide2, n_insilico_ions, rel_intensity_sum, n_successfull_product_ion_matches, insilico_precursor_mass, other_ms1_peak, crosslinker_form, ion_series, isotope_number, td_string, diagnostic_ion_string in results_one_scan:

                    n_ion_series = len(ion_series)
                    if n_ion_series == 0:
                        ion_series = ''
                    else:
                        # sorting of ion_series is needed to make sure that the
                        # string is the same on different operating systems
                        ion_series = str(sorted(list(ion_series)))[1:-1]

                    if len(other_ms1_peak) == 0:
                        other_ms1_peak = ''
                    elif other_ms1_peak == 'no MS1 scan found':
                        pass
                    else:
                        other_ms1_peak = str(other_ms1_peak)[1:-1]

                    delta_ppm = (abs(insilico_precursor_mass
                                     - (isotope_number * ISOTOPE_AVERAGE_DIFFERENCE)
                                     - measured_precursor_mass)
                                 * 1e6 / insilico_precursor_mass)

                    # calculate E value:
                    evalue = self.calc_evalue(
                        n_successfull_product_ion_matches,
                        n_insilico_ions,
                        max_mass_or_mz,
                        n_peaks,
                        measured_precursor_mass,
                        n_theoretical_spectra)

                    if td_string == 'target':
                        tmp_protein1 = fasta.getProteinID(
                            insilico_cl_peptide1.upper(), fasta_info1)
                        tmp_protein2 = fasta.getProteinID(
                            insilico_cl_peptide2.upper(), fasta_info2)
                    else:
                        # decoy
                        tmp_protein1 = [insilico_cl_peptide1]
                        tmp_protein2 = [insilico_cl_peptide2]

                    for protein1 in tmp_protein1:
                        for protein2 in tmp_protein2:

                            if param_decoy_on_demand:
                                if td_string == 'decoy':
                                    protein1 = self.param[
                                        'td_prefix_decoy'] + protein1
                                    protein2 = self.param[
                                        'td_prefix_decoy'] + protein2
                                else:
                                    # target
                                    protein1 = self.param[
                                        'td_prefix_target'] + protein1
                                    protein2 = self.param[
                                        'td_prefix_target'] + protein2

                            output_csv_per_scan.writerow([
                                insilico_cl_peptide1,
                                insilico_cl_peptide2,
                                scan_id,
                                rel_intensity_sum,
                                n_successfull_product_ion_matches,
                                evalue,
                                '{}@{}'.format(protein1, protein2),
                                protein1,
                                protein2,
                                mzml_file.split('/')[-1],
                                insilico_precursor_mass,
                                measured_precursor_mass,
                                measured_precursor_mz,
                                charge,
                                delta_ppm,
                                other_ms1_peak,
                                crosslinker_form,
                                '{0}.{1}.{1}.{2}'.format(mzml_file.split(
                                    '/')[-1], str(scan_id), str(charge)),
                                '',
                                '{}@{}'.format(
                                    insilico_cl_peptide1, insilico_cl_peptide2),
                                ion_series,
                                n_ion_series,
                                isotope_number,
                                diagnostic_ion_string
                            ])

    def calc_evalue(
            self,
            n_successfull_product_ion_matches,
            total_n_calculated_values,
            max_mass_or_mz,
            n_experimental_values,
            precursor_mass,
            n_theoretical_spectra):
        '''
        calculate eValue
        '''
        tolerance = (1e-6
                     * self.param['ppmMS2']
                     * max_mass_or_mz)  # product ion tolerance
        try:
            evalue = spectra.eValue(
                n_successfull_product_ion_matches,
                tolerance,
                total_n_calculated_values,
                n_experimental_values,
                precursor_mass,
                n_theoretical_spectra)
            return evalue
        except ZeroDivisionError:
            return 'n/a'

    def calc_all_fragments(
            self,
            peptide1,
            peptide2,
            crosslinker_mass,
            max_charge,
            internal_precision,
            compiled_re_crosslinked_aa,
            diagnostic_ions=None,
            flag_18o=False):
        '''
        Calculate all fragment ions.
        This function takes all the time.

        peptide1: string containing an amino acid sequence, where the
                  crosslinked amino acid is in lowercase
        peptide2: string containing an amino acid sequence, where the
                  crosslinked amino acid is in lowercase
        both peptides are cross-linked to each other

        crosslinker_mass: mass of the cross-linker (when attached to two
                          peptides), integer with internal_precision
        max_charge: maximum charge state which should be consider, int, 0 is
                    used for deconvoluted spectra
        internal_precision: factor for calculating integers from float

        compiled_re_crosslinked_aa: compiled regular expression for searching
                                    the cross-linked amino acids in the
                                    peptide strings
        '''

        param_c_shift = self.param['cShift']
        param_ions_to_search = self.param['ionsToSearch']
        param_calculate_losses = self.param['calculateLosses']

        fragment_masses_for_crosslinking1 = set()
        fragment_masses_for_crosslinking2 = set()
        # storing of the results, key = fragment mass, value = list of ion
        # names
        fragment_mass_to_ion = defaultdict(list)

        fragments1 = crosslinking.fragmentPeptide(peptide1)
        fragments2 = crosslinking.fragmentPeptide(peptide2)

        # add diagnostic ions
        if diagnostic_ions is not None:
            for mass, name in diagnostic_ions.items():
                fragment_mass_to_ion[mass].append(name)

        # begin fragmentation
        for fragments, fragment_masses_for_crosslinking, chain in (
                (fragments1, fragment_masses_for_crosslinking1, 'alpha'),
                (fragments2, fragment_masses_for_crosslinking2, 'beta')):
            for fragment in fragments:
                # check for crosslinked aa in fragment peptide
                # if crosslinked aa is present, consider fragment for
                # crosslinking
                if compiled_re_crosslinked_aa.search(fragment[0]):
                    for fragment_mass, ion in self.uncharged_fragment_masses(
                            fragment[0],
                            fragment[1],
                            internal_precision,
                            param_ions_to_search,
                            param_c_shift,
                            param_calculate_losses,
                            flag_18o=flag_18o):
                        fragment_masses_for_crosslinking.add(
                            (fragment_mass, ion, fragment[0]))
                else:
                    if max_charge == 0:
                        for fragment_mass, ion in self.uncharged_fragment_masses(
                                fragment[0],
                                fragment[1],
                                internal_precision,
                                param_ions_to_search,
                                param_c_shift,
                                param_calculate_losses,
                                flag_18o=flag_18o):
                            # calculate fragment masses for non-crosslinked
                            # fragments
                            fragment_mass_to_ion[fragment_mass].append(
                                '{0}({1})'.format(ion, chain))
                    else:
                        # charged mz
                        for fragment_mass, ion in crosslinking.chargedFragmentMasses(
                                fragment[0],
                                fragment[1],
                                max_charge,
                                internal_precision,
                                param_ions_to_search,
                                param_c_shift,
                                param_calculate_losses,
                                flag_18o=flag_18o):
                            fragment_mass_to_ion[fragment_mass].append(
                                '{0}({1})'.format(ion, chain))

        for fragment1_mass, ion1, peptide1_ in fragment_masses_for_crosslinking1:
            for fragment2_mass, ion2, peptide2_ in fragment_masses_for_crosslinking2:
                # add the m/z of the current crosslinked peptides to the set
                # fragment_masses
                if max_charge == 0:
                    fragment_mass_to_ion[fragment1_mass
                                         + fragment2_mass
                                         + crosslinker_mass].append(
                                             ion1 + '___' + ion2)
                else:
                    max_fragment_charge = crosslinking.max_charge(
                        peptide1_) + crosslinking.max_charge(peptide2_)
                    if max_fragment_charge > max_charge:
                        max_fragment_charge = max_charge
                    for z in range(1, max_fragment_charge + 1):
                        fragment_mass = (fragment1_mass
                                         + fragment2_mass
                                         + crosslinker_mass
                                         + z * PROTON_MASS_INTERNAL_PRECISION[
                                             internal_precision]) / z
                        fragment_mass_to_ion[fragment_mass].append(
                            ion1 + '___' + ion2 + ' (+{0})'.format(z))

        # crosslinker cleavage
        # whole alpha (or beta) peptide as y- ion due to crosslinker cleavage
        for peptide, ion, terminus, ion_type in (
                (peptide1, 'crosslinker_cleavage_y_ion(alpha)', 'C', 'y'),
                (peptide2, 'crosslinker_cleavage_b_ion(beta)', 'N', 'b'),
                (peptide2, 'crosslinker_cleavage_y_ion(beta)', 'C', 'y'),
                (peptide1, 'crosslinker_cleavage_b_ion(alpha)', 'N', 'b')):
            # this does not consider a, c, x and z ions resulting from
            # crosslinker cleavage
            if max_charge == 0:
                for crosslinker_cleavage_ion, _frag_ion in self.uncharged_fragment_masses(
                        peptide,
                        terminus,
                        internal_precision,
                        ion_type,
                        param_c_shift,
                        calculateLosses=param_calculate_losses,
                        flag_18o=flag_18o):
                    fragment_mass_to_ion[crosslinker_cleavage_ion].append(ion)
            else:
                for crosslinker_cleavage_ion, _frag_ion in crosslinking.chargedFragmentMasses(
                        peptide,
                        terminus,
                        max_charge,
                        internal_precision,
                        ion_type,
                        param_c_shift,
                        calculateLosses=param_calculate_losses,
                        flag_18o=flag_18o):
                    fragment_mass_to_ion[crosslinker_cleavage_ion].append(ion)

        # crosslinked immonium ion
        for peptide, chain, terminus, ion_type in ((peptide1, 'alpha', 'C', 'y'),
                                                   (peptide2, 'beta', 'C', 'y'),
                                                   (peptide2, 'beta', 'C', 'y'),
                                                   (peptide1, 'alpha', 'C', 'y')):
            # this does not consider a, c, x and z ions resulting from
            # crosslinked immonium ion
            for second_chain_ion, _frag_ion in self.uncharged_fragment_masses(
                    peptide,
                    terminus,
                    internal_precision,
                    ion_type, param_c_shift,
                    calculateLosses=param_calculate_losses,
                    flag_18o=flag_18o):
                # due to 'crosslinked_immonium_ion' this yields only once
                for immonium_ion, immonium_type in self.uncharged_fragment_masses(
                        'K',
                        'immonium',
                        internal_precision,
                        None, param_c_shift,
                        calculateLosses=param_calculate_losses,
                        flag_18o=flag_18o):
                    if max_charge == 0:
                        fragment_mass_to_ion[second_chain_ion
                                             + immonium_ion
                                             + crosslinker_mass].append(
                                                 immonium_type
                                                 + '___'
                                                 + peptide)
                    else:
                        max_fragment_charge = crosslinking.max_charge(
                            peptide) + 1
                        for z in range(1, max_fragment_charge + 1):
                            fragment_mz = (second_chain_ion
                                           + immonium_ion
                                           + crosslinker_mass
                                           + z * PROTON_MASS_INTERNAL_PRECISION[
                                               internal_precision]) / z
                            fragment_mass_to_ion[fragment_mz].append(
                                immonium_type
                                + '___'
                                + peptide
                                + ' (+{0})'.format(z))

        return fragment_mass_to_ion

    def highest_peaks(self, spectrum_mass_tuple, number=3):
        '''
        Identify the three highes peaks in a spectrum.
        Returns a set of containing all masses with error
        '''

        precision_ms2 = self.param['ppmMS2'] * 1e-6
        precision_ms2_internal = self.precision_ms2_internal

        highest_peaks_p = heapq.nlargest(
            number, spectrum_mass_tuple, key=itemgetter(1))
        highest_peaks = []
        for mass, dummy_i in highest_peaks_p:
            highest_peaks.append(mass)

        highest_peaks_transformed_with_error = set()
        for mass in highest_peaks:
            for t_mass_with_error in range(
                    int(round((mass - (mass * precision_ms2)) * precision_ms2_internal)),
                    int(round((mass + (mass * precision_ms2)) * precision_ms2_internal)) + 1):
                highest_peaks_transformed_with_error.add(
                    t_mass_with_error)
        return highest_peaks_transformed_with_error

    def internal_precision(self):
        '''
        get internal precisions from MS1 and MS2 spectra.
        '''
        param_precision_ms1 = self.param['ppmMS1'] * 1e-6
        param_precision_ms2 = self.param['ppmMS2'] * 1e-6
        first = True
        for mzml_file in self.input['mzMLFiles']:
            for s in pymzml.run.Reader(
                    mzml_file,
                    MS1_Precision=param_precision_ms1,
                    MSn_Precision=param_precision_ms2):
                if first and s['ms level'] == 1:
                    precision_ms1_internal = s.internalPrecision
                    first = False
                if s['ms level'] == 2:
                    precision_ms2_internal = s.internalPrecision
                    break
            break
        try:
            type(precision_ms1_internal)
        except UnboundLocalError:
            print('No MS1 scans were found.')
            print('Those are needed for identifying a crosslinker mass shift.')

            # escape precision_ms1_internal when no MS1 are found
            for mzml_file in self.input['mzMLFiles']:
                for s in pymzml.run.Reader(
                        mzml_file,
                        MS1_Precision=param_precision_ms1,
                        MSn_Precision=param_precision_ms1):
                    if s['ms level'] == 2:
                        precision_ms1_internal = s.internalPrecision
                        break
                break

        print('Using internal precisions: {} (MS1), {} (MS2)'.format(
            precision_ms1_internal, precision_ms2_internal))
        return precision_ms1_internal, precision_ms2_internal

    def get_all_peptides(self, fasta_files):
        '''
        Get all peptides that can be cross-linked.
        returning allpeptides, peptide2mass
        '''
        allpeptides = set()
        fasta_info = list()

        for fasta_file in fasta_files:
            allpeptides |= crosslinking.getPeptidesFromFasta(
                fasta_file,
                self.param['missedCleavageSites'],
                self.param['enzyme'],
                self.param['notCleavedAminoAcids'],
                self.param['minPeptideLength'])
            for tuple_element in fasta.parseFasta(
                    open(fasta_file), NTerminus=True):
                fasta_info.append(tuple_element)

        # allpeptide now contains a set of all peptides (digested, with missed
        # cleavages, not cleaved after amino acid, minimum peptide length)

        # get all single peptide masses from fastaFilesA
        temp = crosslinking.deletePeptidesAndCalculateMass(
            allpeptides,
            self.crosslinked_aa,
            self.param['cShift'],
            self.precision_ms1_internal,
            flag_18o=self.param['18O'])
        # now without peptides which don't contain the crosslinked amino acid
        sys.stdout.flush()

        # returning allpeptides, peptide2mass
        return temp[0], fasta_info, temp[2]

    def diagnostic_ions(self, crosslinkermassshift):
        '''
        Calculate diagnostic ions.
        Returns a dict, containing heavy and light diagnostic ions whith
        and whithout internal precision
        '''
        diagnostic_ions = defaultdict(dict)
        diagnostic_ions['light no internal'] = {
            222.1488554: 'diagn. 222.1488554',
            239.1754045: 'diagn. 239.1754045',
            305.2223547: 'diagn. 305.2223547'}
        for mass_light, _name in diagnostic_ions['light no internal'].items():
            # get heavy masses
            mass_heavy = mass_light + crosslinkermassshift
            diagnostic_ions['heavy no internal'][
                mass_heavy] = 'diagn. {0}'.format(mass_heavy)

            # transform light to internal precision
            mass_light_internal = crosslinking.internal_precision(
                mass_light, self.precision_ms2_internal)
            diagnostic_ions['light'][
                mass_light_internal] = 'diagn. {0}'.format(mass_light_internal)

            # transform heavy to internal precision
            mass_heavy_internal = crosslinking.internal_precision(
                mass_heavy, self.precision_ms2_internal)
            diagnostic_ions['heavy'][
                mass_heavy_internal] = 'diagn. {0}'.format(mass_heavy_internal)

        return diagnostic_ions

    def delete_big_peptides(self, peptide_mass_list):
        '''
        find peptides, which are too big
        '''
        # sort peptide_mass_list
        peptide_mass_list = sorted(peptide_mass_list)

        max_pos = None
        for pos, (mass, _peptide) in enumerate(peptide_mass_list):
            if mass > 16000 * self.precision_ms1_internal:
                print(
                    'Found {}/{} which are too big, even if they are not crosslinked (max mass = 2000 * 8)'.format(
                        len(peptide_mass_list) - pos,
                        len(peptide_mass_list)))
                sys.stdout.flush()
                max_pos = pos
                break

        # delete peptides, which are too big
        if max_pos is not None:
            peptide_mass_list = peptide_mass_list[:max_pos]
        return peptide_mass_list

    def input_reader(self, input_files):
        '''
        Read OMSSA or Crosslinx input files.
        '''
        input_dict = defaultdict(set)
        crosslink_dict_peptide_combination = defaultdict(dict)
        # read OMSSA files:
        if len(input_files) != 0:
            for input_file in input_files:
                omssa_reader = csv.reader(open(input_file))
                header = next(omssa_reader)
                for line_array in omssa_reader:
                    final_check = False
                    for column in [' Filename/id', 'Filename/id', 'filename/id']:
                        try:
                            filename_id_string = line_array[header.index(column)]
                            final_check = True
                        except ValueError:
                            pass
                    if final_check is False:
                        print('column not found')
                        raise ValueError
                    if filename_id_string.endswith('.dta'):
                        filename_id_string = filename_id_string[:-4]
                        filename_id_string = ''.join(
                            filename_id_string.split('\\\\')[-1])
                    # omssa_charge = filename_id_string.split('.')[-1]
                    omssa_scan1 = filename_id_string.split('.')[-2]
                    omssa_scan2 = filename_id_string.split('.')[-3]
                    omssa_mzml = filename_id_string.split('.')[0]
                    if omssa_scan1 != omssa_scan2:
                        continue
                    input_dict[omssa_mzml].add(int(omssa_scan1))

                    # for crosslink input files
                    if 'insilico_cl_peptide1' in header and 'insilico_cl_peptide2' in header:
                        peptide1 = line_array[
                            header.index('insilico_cl_peptide1')].upper()
                        peptide2 = line_array[
                            header.index('insilico_cl_peptide2')].upper()
                        peptide_combination = (peptide1, peptide2)
                        if self.param['plot']:
                            # read the peptide combination for plotting, only that
                            # peptide combination is plotted later on
                            if int(omssa_scan1) not in crosslink_dict_peptide_combination[
                                    omssa_mzml].keys():
                                crosslink_dict_peptide_combination[
                                    omssa_mzml][int(omssa_scan1)] = set()
                            crosslink_dict_peptide_combination[omssa_mzml][
                                int(omssa_scan1)].add(peptide_combination)

        return input_dict, crosslink_dict_peptide_combination

if __name__ == '__main__':
    # pylint: disable=invalid-name
    script = Crosslinx()
